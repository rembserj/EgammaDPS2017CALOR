import FWCore.ParameterSet.Config as cms

# from RecoEgamma.ElectronIdentification.ElectronMVAValueMapProducer_cfi import electronMVAVariableHelper
from PhysicsTools.SelectorUtils.tools.vid_id_tools import *
from Configuration.AlCa.GlobalTag import GlobalTag

process = cms.Process("ElectronNtuplizer")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")

process.GlobalTag = GlobalTag(process.GlobalTag, "auto:run2_mc", "")

# File with the ID variables to include in the Ntuplizer
mvaVariablesFile = "RecoEgamma/ElectronIdentification/data/ElectronIDVariables.txt"

outputFile = "electron_ntuple.root"

process.maxEvents = cms.untracked.PSet(input=cms.untracked.int32(-1))

process.source = cms.Source(
    "PoolSource",
    fileNames=cms.untracked.vstring(
        "file:/home/llr/cms/rembser/data/store/mc/RunIIFall17MiniAODv2/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14_ext1-v1/70000/3C48E19F-BD43-E811-A110-001E67792594.root"
    ),
)

useAOD = False

from PhysicsTools.SelectorUtils.tools.vid_id_tools import *

# turn on VID producer, indicate data format  to be
# DataFormat.AOD or DataFormat.MiniAOD, as appropriate
if useAOD == True:
    dataFormat = DataFormat.AOD
else:
    dataFormat = DataFormat.MiniAOD

switchOnVIDElectronIdProducer(process, dataFormat)

# define which IDs we want to produce
my_id_modules = [
    "RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Fall17_noIso_V1_cff",
    "RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Fall17_iso_V1_cff",
    "RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Fall17_noIso_V2_cff",
    "RecoEgamma.ElectronIdentification.Identification.mvaElectronID_Fall17_iso_V2_cff",
    # 'RecoEgamma.ElectronIdentification.Identification.cutBasedElectronID_Fall17_94X_V1_cff',
]

# add them to the VID producer
for idmod in my_id_modules:
    setupAllVIDIdsInModule(process, idmod, setupVIDElectronSelection)

process.ntuplizer = cms.EDAnalyzer(
    "ElectronNtuplizer",
    # AOD case
    src=cms.InputTag("gedGsfElectrons"),
    vertices=cms.InputTag("offlinePrimaryVertices"),
    pileup=cms.InputTag("addPileupInfo"),
    genParticles=cms.InputTag("genParticles"),
    MET=cms.InputTag("pfMet"),
    genJets=cms.InputTag("ak4GenJets"),
    # miniAOD case
    srcMiniAOD=cms.InputTag("slimmedElectrons"),
    verticesMiniAOD=cms.InputTag("offlineSlimmedPrimaryVertices"),
    pileupMiniAOD=cms.InputTag("slimmedAddPileupInfo"),
    genParticlesMiniAOD=cms.InputTag("prunedGenParticles"),
    METMiniAOD=cms.InputTag("slimmedMETs"),
    genJetsMiniAOD=cms.InputTag("slimmedGenJets"),
    #
    eleMVAs=cms.vstring(
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V1-wp90",
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V1-wp80",
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V1-wpLoose",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V1-wp90",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V1-wp80",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V1-wpLoose",
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V2-wp90",
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V2-wp80",
        "egmGsfElectronIDs:mvaEleID-Fall17-noIso-V2-wpLoose",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V2-wp90",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V2-wp80",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V2-wpLoose",
        "egmGsfElectronIDs:mvaEleID-Fall17-iso-V2-wpHZZ",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V1-veto",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V1-loose",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V1-medium",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V1-tight",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V2-veto",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V2-loose",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V2-medium",
        # "egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V2-tight",
    ),
    eleMVALabels=cms.vstring(
        "Fall17noIsoV1wp90",
        "Fall17noIsoV1wp80",
        "Fall17noIsoV1wpLoose",
        "Fall17isoV1wp90",
        "Fall17isoV1wp80",
        "Fall17isoV1wpLoose",
        "Fall17noIsoV2wp90",
        "Fall17noIsoV2wp80",
        "Fall17noIsoV2wpLoose",
        "Fall17isoV2wp90",
        "Fall17isoV2wp80",
        "Fall17isoV2wpLoose",
        "Fall17isoV2wpHZZ",
        # "Fall17V1veto",
        # "Fall17V1loose",
        # "Fall17V1medium",
        # "Fall17V1tight",
        # "Fall17V2veto",
        # "Fall17V2loose",
        # "Fall17V2medium",
        # "Fall17V2tight",
    ),
    eleMVAValMaps=cms.vstring(
        "electronMVAValueMapProducer:ElectronMVAEstimatorRun2Fall17IsoV1RawValues",
        "electronMVAValueMapProducer:ElectronMVAEstimatorRun2Fall17NoIsoV1RawValues",
        "electronMVAValueMapProducer:ElectronMVAEstimatorRun2Fall17IsoV2RawValues",
        "electronMVAValueMapProducer:ElectronMVAEstimatorRun2Fall17NoIsoV2RawValues",
    ),
    eleMVAValMapLabels=cms.vstring("Fall17IsoV1Val", "Fall17NoIsoV1Val", "Fall17IsoV2Val", "Fall17NoIsoV2Val"),
    eleMVACats=cms.vstring("electronMVAValueMapProducer:ElectronMVAEstimatorRun2Fall17NoIsoV1Categories"),
    eleMVACatLabels=cms.vstring("EleMVACat"),
    #
    variableDefinition=cms.string(mvaVariablesFile),
    isMC=cms.bool(True),
    deltaR=cms.double(0.1),
)

process.TFileService = cms.Service("TFileService", fileName=cms.string(outputFile))

# process.electronMVAVariableHelper = electronMVAVariableHelper
process.p = cms.Path(process.egmGsfElectronIDSequence * process.ntuplizer)
