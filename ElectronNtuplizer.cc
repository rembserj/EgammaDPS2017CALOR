// -*- C++ -*-
//
// Package:    Guitargeek/Ntuplizer
// Class:      ElectronNtuplizer
//
/**\class ElectronNtuplizer ElectronNtuplizer.cc Guitargeek/Ntuplizer/plugins/ElectronNtuplizer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Jonas REMBSER
//         Created:  Thu, 22 Mar 2018 14:54:24 GMT
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/JetReco/interface/GenJet.h"

#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/PatCandidates/interface/Electron.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"

#include <DataFormats/METReco/interface/PFMET.h>
#include <DataFormats/METReco/interface/PFMETCollection.h>
#include <DataFormats/PatCandidates/interface/MET.h>

#include "TTree.h"
#include "TFile.h"
#include "Math/VectorUtil.h"

//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.
//

class ElectronNtuplizer : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
   public:
      explicit ElectronNtuplizer(const edm::ParameterSet&);
      ~ElectronNtuplizer();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


   private:
      virtual void beginJob() override;
      virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
      virtual void endJob() override;

      void findFirstNonElectronMother2(const reco::Candidate *particle, int &ancestorPID, int &ancestorStatus);
      double getEffArea(float sclEta, int year);

      template<class T, class V>
      int matchToTruth(const T &el, const V &genParticles, int &genIdx);
      template<class T, class V>
      int matchToGenJets(const T el, const V genJets);

      // ----------member data ---------------------------

      // for AOD case
      edm::EDGetToken src_;
      edm::EDGetToken vertices_;
      edm::EDGetToken pileup_;
      edm::EDGetToken genParticles_;
      edm::EDGetToken metToken_;
      edm::EDGetToken genJetsToken_;

      // for miniAOD case
      edm::EDGetToken srcMiniAOD_;
      edm::EDGetToken verticesMiniAOD_;
      edm::EDGetToken pileupMiniAOD_;
      edm::EDGetToken genParticlesMiniAOD_;
      edm::EDGetToken metTokenMiniAOD_;
      edm::EDGetToken genJetsTokenMiniAOD_;

      edm::EDGetToken rhoToken_;

      // other
      TTree* eventTree_;
      TTree* recoEleTree_;

      std::vector<float> vars_;
      int nVars_;

      //global variables
      int nEvent_, nRun_, nLumi_;
      int genNpu_;
      int vtxN_;
      float met_;
      float rho_;
      int nElectrons_;
      int nElectronsBkg_;
      int nElectronsSig_;

      // electron variables
      float eleQ_;
      int ele3Q_;
      float elePt_;
      float sclEta_;

      int matchedToGenEle_;
      int matchedGenEleIdx_;
      int matchedToGenJet_;
      int matchedGenJetIdx_;

      float elePfChargedHadIso_, elePfNeutralHadIso_, elePfPhotonIso_, elePfChargedIso_, elePfSumPUIso_;
      float elePfCombRelIso2016_;
      float elePfCombRelIso2017_;

      // ecal driven-tracker driven variables
      bool eleIsEcalDriven_;
      bool eleIsTrackerDriven_;

      // gap variables
      bool eleIsEB_;
      bool eleIsEE_;
      bool eleIsEBEtaGap_;
      bool eleIsEBPhiGap_;
      bool eleIsEBEEGap_;
      bool eleIsEEDeeGap_;
      bool eleIsEERingGap_;

      // to hold ID decisions and categories
      std::vector<int> mvaPasses_;
      std::vector<float> mvaValues_;
      std::vector<int> mvaCats_;

      // config
      const bool isMC_;
      const double deltaR_;
      const double deltaRJets_;
      //const bool saveIDVariables_;
      const double ptThreshold_;

      // ID decisions objects
      const std::vector< std::string > eleMapTags_;
      std::vector< edm::EDGetTokenT< edm::ValueMap<bool> > > eleMapTokens_;
      const std::vector< std::string > eleMapBranchNames_;
      const size_t nEleMaps_;

      // MVA values and categories (optional)
      const std::vector< std::string > valMapTags_;
      std::vector< edm::EDGetTokenT<edm::ValueMap<float> > > valMapTokens_;
      const std::vector< std::string > valMapBranchNames_;
      const size_t nValMaps_;

      const std::vector< std::string > mvaCatTags_;
      std::vector< edm::EDGetTokenT<edm::ValueMap<int> > > mvaCatTokens_;
      const std::vector< std::string > mvaCatBranchNames_;
      const size_t nCats_;
};

//
// constants, enums and typedefs
//

enum ElectronMatchType {
                        UNMATCHED,
                        TRUE_PROMPT_ELECTRON,
                        TRUE_ELECTRON_FROM_TAU,
                        TRUE_NON_PROMPT_ELECTRON,
                        B_QUARK,
                       }; // The last does not include tau parents

//
// static data member definitions
//

//
// constructors and destructor
//
ElectronNtuplizer::ElectronNtuplizer(const edm::ParameterSet& iConfig)
 :
  src_                   (consumes<edm::View<reco::GsfElectron> >(iConfig.getParameter<edm::InputTag>("src"))),
  vertices_              (consumes<std::vector<reco::Vertex> >(iConfig.getParameter<edm::InputTag>("vertices"))),
  pileup_                (consumes<std::vector< PileupSummaryInfo > >(iConfig.getParameter<edm::InputTag>("pileup"))),
  genParticles_          (consumes<edm::View<reco::GenParticle> >(iConfig.getParameter<edm::InputTag>("genParticles"))),
  metToken_              (consumes<pat::METCollection >(iConfig.getParameter<edm::InputTag>("MET"))),
  genJetsToken_          (consumes<std::vector<reco::GenJet>>(iConfig.getParameter<edm::InputTag>("genJets"))),
  srcMiniAOD_            (consumes<edm::View<reco::GsfElectron> >(iConfig.getParameter<edm::InputTag>("srcMiniAOD"))),
  verticesMiniAOD_       (consumes<std::vector<reco::Vertex> >(iConfig.getParameter<edm::InputTag>("verticesMiniAOD"))),
  pileupMiniAOD_         (consumes<std::vector< PileupSummaryInfo > >(iConfig.getParameter<edm::InputTag>("pileupMiniAOD"))),
  genParticlesMiniAOD_   (consumes<edm::View<reco::GenParticle> >(iConfig.getParameter<edm::InputTag>("genParticlesMiniAOD"))),
  metTokenMiniAOD_       (consumes<pat::METCollection>(iConfig.getParameter<edm::InputTag>("METMiniAOD"))),
  genJetsTokenMiniAOD_   (consumes<std::vector<reco::GenJet>>(iConfig.getParameter<edm::InputTag>("genJetsMiniAOD"))),
  rhoToken_              (consumes<double>(edm::InputTag("fixedGridRhoFastjetAll",""))),
  isMC_                  (iConfig.getParameter<bool>("isMC")),
  deltaR_                (iConfig.existsAs<double>("deltaR")          ? iConfig.getParameter<double>("deltaR"): 0.1),
  deltaRJets_            (iConfig.existsAs<double>("deltaRJets")      ? iConfig.getParameter<double>("deltaRJets"): 0.1),
  //saveIDVariables_       (iConfig.existsAs<double>("saveIDVariables") ? iConfig.getParameter<double>("saveIDVariables"): true),
  ptThreshold_           (iConfig.existsAs<double>("ptThreshold")     ? iConfig.getParameter<double>("ptThreshold"): 5),
  eleMapTags_            (iConfig.getParameter<std::vector<std::string>>("eleMVAs")),
  eleMapBranchNames_     (iConfig.getParameter<std::vector<std::string>>("eleMVALabels")),
  nEleMaps_              (eleMapBranchNames_.size()),
  valMapTags_            (iConfig.getParameter<std::vector<std::string>>("eleMVAValMaps")),
  valMapBranchNames_     (iConfig.getParameter<std::vector<std::string>>("eleMVAValMapLabels")),
  nValMaps_              (valMapBranchNames_.size()),
  mvaCatTags_            (iConfig.getParameter<std::vector<std::string>>("eleMVACats")),
  mvaCatBranchNames_     (iConfig.getParameter<std::vector<std::string>>("eleMVACatLabels")),
  nCats_                 (mvaCatBranchNames_.size())
{
    // eleMaps
    for (size_t k = 0; k < nEleMaps_; ++k) {

        eleMapTokens_.push_back(consumes<edm::ValueMap<bool> >(edm::InputTag(eleMapTags_[k])));

        // Initialize vectors for holding ID decisions
        mvaPasses_.push_back(0);
    }

    // valMaps
    for (size_t k = 0; k < nValMaps_; ++k) {
        valMapTokens_.push_back(consumes<edm::ValueMap<float> >(edm::InputTag(valMapTags_[k])));

        // Initialize vectors for holding MVA values
        mvaValues_.push_back(0.0);
    }

    // categories
    for (size_t k = 0; k < nCats_; ++k) {
        mvaCatTokens_.push_back(consumes<edm::ValueMap<int> >(edm::InputTag(mvaCatTags_[k])));

        // Initialize vectors for holding MVA values
        mvaCats_.push_back(0);
    }

   // Book tree

   usesResource(TFileService::kSharedResource);
   edm::Service<TFileService> fs ;

   eventTree_ = fs->make<TTree> ("tree_event", "tree_event");

   eventTree_->Branch("nEvent",  &nEvent_);
   eventTree_->Branch("nRun",    &nRun_);
   eventTree_->Branch("nLumi",   &nLumi_);
   eventTree_->Branch("genNpu", &genNpu_);
   eventTree_->Branch("vtxN",   &vtxN_);
   eventTree_->Branch("rho",   &rho_);
   eventTree_->Branch("nRecoEle",  &nElectrons_);
   eventTree_->Branch("nEleSig",  &nElectronsSig_);
   eventTree_->Branch("nEleBkg",  &nElectronsBkg_);

   recoEleTree_  = fs->make<TTree>("tree","tree");

   recoEleTree_->Branch("nEvent",  &nEvent_);
   recoEleTree_->Branch("nRun",    &nRun_);
   recoEleTree_->Branch("nLumi",   &nLumi_);
   recoEleTree_->Branch("genNpu", &genNpu_);
   recoEleTree_->Branch("vtxN",   &vtxN_);
   recoEleTree_->Branch("rho",   &rho_);
   recoEleTree_->Branch("met",   &met_);

   recoEleTree_->Branch("ele_q",&eleQ_);
   recoEleTree_->Branch("ele_3q",&ele3Q_);

   recoEleTree_->Branch("ele_pt",&elePt_);
   recoEleTree_->Branch("scl_eta",&sclEta_);

    recoEleTree_->Branch("ele_pfChargedHadIso",&elePfChargedHadIso_);
    recoEleTree_->Branch("ele_pfNeutralHadIso",&elePfNeutralHadIso_);
    recoEleTree_->Branch("ele_pfPhotonIso",&elePfPhotonIso_);

    //recoEleTree_->Branch("ele_pfChargedIso", &elePfChargedIso_);
    //recoEleTree_->Branch("ele_pfSumPUIso", &elePfSumPUIso_);
    recoEleTree_->Branch("ele_pfCombRelIso2016", &elePfCombRelIso2016_);
    recoEleTree_->Branch("ele_pfCombRelIso2017", &elePfCombRelIso2017_);

   if (isMC_) {
       recoEleTree_->Branch("matchedToGenEle",   &matchedToGenEle_);
       //recoEleTree_->Branch("matchedToGenJet"  , &matchedToGenJet_);
   }

   // Has to be in two different loops
   for (int i = 0; i < nVars_; ++i) {
       vars_.push_back(0.0);
   }

   recoEleTree_->Branch("ele_isEcalDriven",&eleIsEcalDriven_);
   recoEleTree_->Branch("ele_isTrackerDriven",&eleIsTrackerDriven_);
   recoEleTree_->Branch("ele_isEB",&eleIsEB_);
   recoEleTree_->Branch("ele_isEE",&eleIsEE_);
   recoEleTree_->Branch("ele_isEBEtaGap",&eleIsEBEtaGap_);
   recoEleTree_->Branch("ele_isEBPhiGap",&eleIsEBPhiGap_);
   recoEleTree_->Branch("ele_isEBEEGap", &eleIsEBEEGap_);
   recoEleTree_->Branch("ele_isEEDeeGap",&eleIsEEDeeGap_);
   recoEleTree_->Branch("ele_isEERingGap",&eleIsEERingGap_);

   // IDs
   for (size_t k = 0; k < nValMaps_; ++k) {
       recoEleTree_->Branch(valMapBranchNames_[k].c_str() ,  &mvaValues_[k]);
   }

   for (size_t k = 0; k < nEleMaps_; ++k) {
       recoEleTree_->Branch(eleMapBranchNames_[k].c_str() ,  &mvaPasses_[k]);
   }

   for (size_t k = 0; k < nCats_; ++k) {
       recoEleTree_->Branch(mvaCatBranchNames_[k].c_str() ,  &mvaCats_[k]);
   }

   // All tokens for event content needed by this MVA
   // Tags from the variable helper
}


ElectronNtuplizer::~ElectronNtuplizer()
{

   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
ElectronNtuplizer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

    nElectrons_ = 0;
    nElectronsSig_ = 0;
    nElectronsBkg_ = 0;

    // Fill global event info
    nEvent_ = iEvent.id().event();
    nRun_   = iEvent.id().run();
    nLumi_  = iEvent.luminosityBlock();


    // Retrieve Vertecies
    edm::Handle<reco::VertexCollection> vertices;
    iEvent.getByToken(vertices_, vertices);
    if( !vertices.isValid() ){
      iEvent.getByToken(verticesMiniAOD_,vertices);
      if( !vertices.isValid() )
        throw cms::Exception(" Collection not found: ")
          << " failed to find a standard AOD or miniAOD vertex collection " << std::endl;
    }

    vtxN_ = vertices->size();

    // MET
    edm::Handle<pat::METCollection> metHandle;
    iEvent.getByToken(metTokenMiniAOD_,metHandle);
    //if( !metHandle.isValid() ){
      //iEvent.getByToken(metTokenMiniAOD_,metHandle);
      //if( metHandle.isValid() )
        //throw cms::Exception(" Collection not found: ")
          //<< " failed to find a standard AOD or miniAOD MET collection " << std::endl;
    //}
    const pat::MET &met = metHandle->front();
    met_ = met.pt();

    // Gen Jets
    edm::Handle<std::vector<reco::GenJet>> genJets;
    iEvent.getByToken(genJetsTokenMiniAOD_,genJets);
    //if( !genJets.isValid() ){
      //iEvent.getByToken(genJetsTokenMiniAOD_,genJets);
      //if( genJets.isValid() )
        //throw cms::Exception(" Collection not found: ")
          //<< " failed to find a standard AOD or miniAOD GenJets collection " << std::endl;
    //}

    // Retrieve Pileup Info
    edm::Handle<std::vector< PileupSummaryInfo > >  pileup;
    iEvent.getByToken(pileup_, pileup);
    if( !pileup.isValid() ){
      iEvent.getByToken(pileupMiniAOD_,pileup);
      if( !pileup.isValid() )
        throw cms::Exception(" Collection not found: ")
          << " failed to find a standard AOD or miniAOD pileup collection " << std::endl;
    }

    // rho
    edm::Handle<double> rhoHandle;
    iEvent.getByToken(rhoToken_, rhoHandle);
    rho_ = *rhoHandle;

    // Fill with true number of pileup
    if(isMC_) {
       for(const auto& pu : *pileup)
       {
           int bx = pu.getBunchCrossing();
           if(bx == 0)
           {
               genNpu_ = pu.getPU_NumInteractions();
               break;
           }
       }
    }

    // Retrieve genParticles
    edm::Handle<edm::View<reco::GenParticle> >  genParticles;
    iEvent.getByToken(genParticles_, genParticles);
    if( !genParticles.isValid() ){
      iEvent.getByToken(genParticlesMiniAOD_, genParticles);
      if( !genParticles.isValid() )
        throw cms::Exception(" Collection not found: ")
          << " failed to find a standard AOD or miniAOD genParticle collection " << std::endl;
    }


    edm::Handle<edm::View<reco::GsfElectron> > src;

    // Retrieve the collection of particles from the event.
    // If we fail to retrieve the collection with the standard AOD
    // name, we next look for the one with the stndard miniAOD name.
    iEvent.getByToken(src_, src);
    if( !src.isValid() ){
      iEvent.getByToken(srcMiniAOD_,src);
      if( !src.isValid() )
        throw cms::Exception(" Collection not found: ")
          << " failed to find a standard AOD or miniAOD particle collection " << std::endl;
    }

    // Get MVA decisions
    edm::Handle<edm::ValueMap<bool> > decisions[nEleMaps_];
    for (size_t k = 0; k < nEleMaps_; ++k) {
        iEvent.getByToken(eleMapTokens_[k],decisions[k]);
    }

    // Get MVA values
    edm::Handle<edm::ValueMap<float> > values[nValMaps_];
    for (size_t k = 0; k < nValMaps_; ++k) {
        iEvent.getByToken(valMapTokens_[k],values[k]);
    }

    // Get MVA categories
    edm::Handle<edm::ValueMap<int> > mvaCats[nCats_];
    for (size_t k = 0; k < nCats_; ++k) {
        iEvent.getByToken(mvaCatTokens_[k],mvaCats[k]);
    }

    int nEle = src->size();

    for(int iEle = 0; iEle < nEle; ++iEle) {

        const auto ele =  src->ptrAt(iEle);

        eleQ_ = ele->charge();
        ele3Q_ = ele->chargeInfo().isGsfCtfScPixConsistent;

        elePt_ = ele->pt();
        sclEta_ = ele->superCluster()->eta();

        elePfChargedHadIso_   = (ele->pfIsolationVariables()).sumChargedHadronPt ; //chargedHadronIso();
        elePfNeutralHadIso_   = (ele->pfIsolationVariables()).sumNeutralHadronEt ; //neutralHadronIso();
        elePfPhotonIso_       = (ele->pfIsolationVariables()).sumPhotonEt; //photonIso();

        //elePfChargedIso_      = (ele->pfIsolationVariables()).sumChargedParticlePt;
        //elePfSumPUIso_        = (ele->pfIsolationVariables()).sumPUPt;

        double effArea2016 = getEffArea(sclEta_, 2016);
        double effArea2017 = getEffArea(sclEta_, 2017);
        elePfCombRelIso2016_ = (elePfChargedHadIso_ + std::max(0., elePfNeutralHadIso_ + elePfPhotonIso_ - rho_ * effArea2016))/elePt_;
        elePfCombRelIso2017_ = (elePfChargedHadIso_ + std::max(0., elePfNeutralHadIso_ + elePfPhotonIso_ - rho_ * effArea2017))/elePt_;

        if (ele->pt() < ptThreshold_) {
            continue;
        }

        if (isMC_) {
            matchedToGenEle_ = matchToTruth( ele, genParticles, matchedGenEleIdx_);
            //matchedToGenJet_ = matchToGenJets( ele, genJets );
        }
        if (matchedToGenEle_ == 1) nElectronsSig_++;
        if (matchedToGenEle_ == 0 || matchedToGenEle_ == 3) nElectronsBkg_++;

        eleIsEcalDriven_ = ele->ecalDrivenSeed();
        eleIsTrackerDriven_ = ele->trackerDrivenSeed();

        // gap variables
        eleIsEB_ = ele->isEB();
        eleIsEE_ = ele->isEE();
        eleIsEBEEGap_ = ele->isEBEEGap();
        eleIsEBEtaGap_ = ele->isEBEtaGap();
        eleIsEBPhiGap_ = ele->isEBPhiGap();
        eleIsEEDeeGap_ = ele->isEEDeeGap();
        eleIsEERingGap_ = ele->isEERingGap();

        //
        // Look up and save the ID decisions
        //
        for (size_t k = 0; k < nEleMaps_; ++k) {
          mvaPasses_[k] = (int)(*decisions[k])[ele];
        }

        for (size_t k = 0; k < nValMaps_; ++k) {
          mvaValues_[k] = (*values[k])[ele];
        }

        for (size_t k = 0; k < nCats_; ++k) {
          mvaCats_[k] = (*mvaCats[k])[ele];
        }

        nElectrons_++;

        recoEleTree_->Fill();
    }

    eventTree_->Fill();

}

void ElectronNtuplizer::findFirstNonElectronMother2(const reco::Candidate *particle,
                         int &ancestorPID, int &ancestorStatus){

  if( particle == 0 ){
    printf("ElectronNtupler: ERROR! null candidate pointer, this should never happen\n");
    return;
  }

  // Is this the first non-electron parent? If yes, return, otherwise
  // go deeper into recursion
  if( abs(particle->pdgId()) == 11 ){
    findFirstNonElectronMother2(particle->mother(0), ancestorPID, ancestorStatus);
  }else{
    ancestorPID = particle->pdgId();
    ancestorStatus = particle->status();
  }

  return;
}

template<class T, class V>
int ElectronNtuplizer::matchToTruth(const T &el, const V &prunedGenParticles, int &genIdx){

  //
  // Explicit loop and geometric matching method (advised by Josh Bendavid)
  //

  // Find the closest status 1 gen electron to the reco electron
  double dR = 999;
  const reco::Candidate *closestElectron = 0;
  bool matchedToBQuark_ = false;
  for(size_t i=0; i<prunedGenParticles->size();i++){
    const reco::Candidate *particle = &(*prunedGenParticles)[i];
    // Drop everything that is not electron or not status 1 or prompt bquark
    if( (abs(particle->pdgId()) != 11 || particle->status() != 1) &&
        (abs(particle->pdgId()) != 5 || particle->status() != 23) )
      continue;
    //
    double dRtmp = ROOT::Math::VectorUtil::DeltaR( el->p4(), particle->p4() );
    if( dRtmp < dR ){
      dR = dRtmp;
      closestElectron = particle;
      genIdx = i;
      if (abs(particle->pdgId()) == 5)  matchedToBQuark_ = true;
      else matchedToBQuark_ = false;
    }
  }
  // See if the closest electron (if it exists) is close enough.
  // If not, no match found.
  if( !(closestElectron != 0 && dR < deltaR_ + 0.3 * matchedToBQuark_) ) { // so it's 0.4 for matching to b-quark
    return UNMATCHED;
  }

  if (matchedToBQuark_) {
      return B_QUARK;
  }

  //
  int ancestorPID = -999;
  int ancestorStatus = -999;
  findFirstNonElectronMother2(closestElectron, ancestorPID, ancestorStatus);

  if( ancestorPID == -999 && ancestorStatus == -999 ){
    // No non-electron parent??? This should never happen.
    // Complain.
    printf("ElectronNtupler: ERROR! Electron does not apper to have a non-electron parent\n");
    return UNMATCHED;
  }

  if( abs(ancestorPID) > 50 && ancestorStatus == 2 )
    return TRUE_NON_PROMPT_ELECTRON;

  if( abs(ancestorPID) == 15 && ancestorStatus == 2 )
    return TRUE_ELECTRON_FROM_TAU;

  // What remains is true prompt electrons
  return TRUE_PROMPT_ELECTRON;
}

template<class T, class V>
int ElectronNtuplizer::matchToGenJets(const T el, const V genJets){

  //
  // Explicit loop and geometric matching method (advised by Josh Bendavid)
  //

  double dR = 999;
  const reco::Candidate *closestGenJet = 0;
  //int closestGenJetIdx = -1;
  for(size_t i=0; i<genJets->size();i++){
    const reco::Candidate *genJet = &(*genJets)[i];
    //// Drop jets below pt 20 GeV
    //if( genJet->pt() < 20 )
      //continue;
    //
    double dRtmp = ROOT::Math::VectorUtil::DeltaR( el->p4(), genJet->p4() );
    if( dRtmp < dR ){
      dR = dRtmp;
      closestGenJet = genJet;
      //isBJet = genJet->hadronFlavour() == 5;
      //closestGenJetIdx = i;
    }
  }
  // See if the closest gen jet (if it exists) is close enough.
  // If not, no match found.
  if( closestGenJet != 0 && dR < deltaRJets_ ) {
    return 1;
    //return closestGenJetIdx;
  } else {
    return 0;
    //return -1;
  }
}

// ------------ method called once each job just before starting event loop  ------------
void
ElectronNtuplizer::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void
ElectronNtuplizer::endJob()
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
ElectronNtuplizer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addDefault(desc);
}

double ElectronNtuplizer::getEffArea(float sclEta, int year) {

  float absSclEta = fabs(sclEta);

  double effArea = 0;

  // https://github.com/cms-sw/cmssw/blob/master/RecoEgamma/ElectronIdentification/data/Fall17/effAreaElectrons_cone03_pfNeuHadronsAndPhotons_92X.txt
  if (year == 2017) {
        if (absSclEta >= 0.0    && absSclEta < 1.     ) effArea = 0.1566;
        if (absSclEta >= 1.     && absSclEta < 1.479  ) effArea = 0.1626;
        if (absSclEta >= 1.479  && absSclEta < 2.0    ) effArea = 0.1073;
        if (absSclEta >= 2.0    && absSclEta < 2.2    ) effArea = 0.0854;
        if (absSclEta >= 2.2    && absSclEta < 2.3    ) effArea = 0.1051;
        if (absSclEta >= 2.3    && absSclEta < 2.4    ) effArea = 0.1204;
        if (absSclEta >= 2.4 )                          effArea = 0.1524;
  }

  // https://github.com/cms-sw/cmssw/blob/master/RecoEgamma/ElectronIdentification/data/Summer16/effAreaElectrons_cone03_pfNeuHadronsAndPhotons_80X.txt
  else if (year == 2016) {
        if (absSclEta >= 0.0    && absSclEta < 1.     ) effArea = 0.1703;
        if (absSclEta >= 1.     && absSclEta < 1.479  ) effArea = 0.1715;
        if (absSclEta >= 1.479  && absSclEta < 2.0    ) effArea = 0.1213;
        if (absSclEta >= 2.0    && absSclEta < 2.2    ) effArea = 0.1230;
        if (absSclEta >= 2.2    && absSclEta < 2.3    ) effArea = 0.1635;
        if (absSclEta >= 2.3    && absSclEta < 2.4    ) effArea = 0.1937;
        if (absSclEta >= 2.4 )                          effArea = 0.2393;
  }

  return effArea;
}

//define this as a plug-in
DEFINE_FWK_MODULE(ElectronNtuplizer);
