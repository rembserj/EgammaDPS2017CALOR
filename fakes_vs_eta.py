import ROOT
import tdrstyle
import numpy as np
from CMS_lumi import CMS_lumi
from root_numpy import tree2array
import random
import argparse

parser = argparse.ArgumentParser()

# parser.add_argument('--fakes', help='the type of fakes', default="DYJets")
parser.add_argument("--stop", type=int, help="how many electrons to read from each ntuple", default=None)

# parser.print_help()

args = parser.parse_args()

root_file_name_2016 = "ntuples/electron_ntuple_2016_DYJets.root"
root_file_name_2017 = "ntuples/electron_ntuple_2017_DYJets.root"

# Globals for plotting

tdrstyle.set()

writeExtraText = True  # if extra text
extraText = "Preliminary"  # default extra text is "Preliminary"

iPeriod = 3  # 1=7TeV, 2=8TeV, 3=7+8TeV, 7=7+8+13TeV, 0=free form (uses lumi_sqrtS)

W = 800
H = 600

W_ref = 800
H_ref = 600

# references for T, B, L, R
T = 0.08 * H_ref
B = 0.12 * H_ref
L = 0.12 * W_ref
R = 0.04 * W_ref


def setup_legend(legend):
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextAngle(0)
    legend.SetTextColor(ROOT.kBlack)
    # legend.SetTextSize(0.04)
    legend.SetTextSize(0.05)
    legend.SetTextAlign(12)


dummies = []


def add_legend_item(
    legend,
    label="",
    option="l",
    draw_opts="p",
    marker_size=1,
    marker_style=20,
    line_width=1,
    line_color=1,
    line_style=1,
    fill_color=1,
):
    random_name = "%032x" % random.getrandbits(128)
    dummies.append(ROOT.TGraph(1, np.zeros(1, dtype=np.float), np.zeros(1, dtype=np.float)))
    dummies[-1].SetName(random_name)
    dummies[-1].Draw(draw_opts)
    dummies[-1].SetMarkerStyle(marker_style)
    dummies[-1].SetLineWidth(line_width)
    dummies[-1].SetLineColor(line_color)
    dummies[-1].SetLineStyle(line_style)
    dummies[-1].SetFillColor(fill_color)
    dummies[-1].SetMarkerSize(marker_size)
    dummies[-1].Draw(draw_opts)
    legend.AddEntry(random_name, label, option)


##########
# Get Data
##########

root_file_2016 = ROOT.TFile(root_file_name_2016, "READ")
root_dir_2016 = root_file_2016.Get("ntuplizer")
root_tree_2016 = root_dir_2016.Get("tree")
root_tree_event_2016 = root_dir_2016.Get("tree_event")

root_file_2017 = ROOT.TFile(root_file_name_2017, "READ")
root_dir_2017 = root_file_2017.Get("ntuplizer")
root_tree_2017 = root_dir_2017.Get("tree")
root_tree_event_2017 = root_dir_2017.Get("tree_event")

n_events_2016 = root_tree_event_2016.GetEntries("genNpu == 35")
n_events_2017 = root_tree_event_2017.GetEntries("genNpu == 35")

n_fakes_2016 = root_tree_2016.GetEntries("matchedToGenEle != 1 && matchedToGenEle != 2 && genNpu == 35")
n_fakes_2017 = root_tree_2017.GetEntries("matchedToGenEle != 1 && matchedToGenEle != 2 && genNpu == 35")

# hprof_2016 = ROOT.TProfile("hprof2016","nBkg vs scl_eta",80,0.,80.,0.,10.);
# hprof_2017 = ROOT.TProfile("hprof2017","nBkg vs scl_eta",80,0.,80.,0.,10.);

event_npu_2016 = ROOT.TH1F("npu2016", "npu2016", 80, 0.0, 80.0)
event_npu_2017 = ROOT.TH1F("npu2017", "npu2017", 80, 0.0, 80.0)
# bkg_npu_2016 = ROOT.TH1F("bkg2016","bkg2016",80,0.,80.);
# bkg_npu_2017 = ROOT.TH1F("bkg2017","bkg2017",80,0.,80.);
bkg_npu_2016 = ROOT.TH1F("bkg2016", "bkg2016", 50, -2.5, 2.5)
bkg_npu_2017 = ROOT.TH1F("bkg2017", "bkg2017", 50, -2.5, 2.5)

ROOT.gROOT.SetBatch(True)

# root_tree_event_2016.Draw("scl_eta>>npu2016","","")
# root_tree_event_2017.Draw("scl_eta>>npu2017","","")
root_tree_2016.Draw(
    "scl_eta>>bkg2016", "matchedToGenEle != 1 && matchedToGenEle != 2 && genNpu == 35"
)  # && ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) < 2.5","")
root_tree_2017.Draw(
    "scl_eta>>bkg2017", "matchedToGenEle != 1 && matchedToGenEle != 2 && genNpu == 35"
)  # && ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) < 2.5","")

bkg_npu_2016.Sumw2()
bkg_npu_2017.Sumw2()

bkg_npu_2016.Scale(1.0 / n_events_2016)
bkg_npu_2017.Scale(1.0 / n_events_2017)

# bkg_npu_2016.Divide(event_npu_2016)
# bkg_npu_2017.Divide(event_npu_2017)

##########
# Plotting
##########


canv_name = "n_fakes_vs_eta_pt_5_PU35"

canv = ROOT.TCanvas(canv_name, canv_name, 50, 50, W, H)
canv.SetFillColor(0)
canv.SetBorderMode(0)
canv.SetFrameFillStyle(0)
canv.SetFrameBorderMode(0)
canv.SetLeftMargin(L / W)
canv.SetRightMargin(R / W)
canv.SetTopMargin(T / H)
canv.SetBottomMargin(B / H)
canv.SetTickx(0)
canv.SetTicky(0)

canv.DrawFrame(0.0, 80.0, 0.0, 10.0)

bkg_npu_2016.SetMarkerColor(30)
bkg_npu_2017.SetMarkerColor(38)
bkg_npu_2016.SetLineColor(30)
bkg_npu_2017.SetLineColor(38)

# bkg_npu_2016.GetXaxis().SetTitle("NPU")
bkg_npu_2016.GetXaxis().SetTitle("scl_eta")
bkg_npu_2016.GetYaxis().SetTitleOffset(1)
bkg_npu_2016.GetYaxis().SetTitle("DY+Jets Fakes per Event")
bkg_npu_2016.GetYaxis().SetRangeUser(0, 0.01)

bkg_npu_2016.Draw("E")
bkg_npu_2017.Draw("ESAME")

legend = ROOT.TLegend(0.55, 0.7, 0.83, 0.88)
setup_legend(legend)
legend.AddEntry("bkg2016", "2016", "pe")
legend.AddEntry("bkg2017", "2017", "pe")
legend.Draw()

# writing the lumi information and the CMS "logo"
# CMS_lumi( canv, extraText="Simulation Preliminary", iPosX=11)

canv.Update()
canv.RedrawAxis()
canv.GetFrame().Draw()

canv.SetTicky()
canv.SetTickx()

canv.Draw()

canv.SaveAs("plots/number_of_fakes/" + canv_name + ".eps", ".eps")
canv.SaveAs("plots/number_of_fakes/" + canv_name + ".png", ".png")
