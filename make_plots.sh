#!/bin/bash

mkdir -p plots

export NTUPLE_DIR=/eos/user/r/rembserj/ntuples/egamma_DPS_2017

STOP=-1
#STOP=3000000

python roc_curves.py --fakes DYJets --stop $STOP
python roc_curves.py --fakes DYJets --stop $STOP --year 2016
python roc_curves.py --fakes TTJets --bkg_sel "matchedToGenEle == 3" --stop $STOP --ylabel "TT+Jets Non-prompt Efficiency" --suffix non_prompt

python roc_curves.py --fakes DYJets --stop $STOP --lowpt --nocutwp
python roc_curves.py --fakes DYJets --stop $STOP --lowpt --nocutwp --year 2016
