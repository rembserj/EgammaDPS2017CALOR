import ROOT
import tdrstyle
import numpy as np
from CMS_lumi import CMS_lumi
from root_numpy import tree2array
import random
import argparse
import uproot

parser = argparse.ArgumentParser()

# parser.add_argument('--fakes', help='the type of fakes', default="DYJets")
parser.add_argument("--stop", type=int, help="how many electrons to read from each ntuple", default=None)
parser.add_argument("--ul2017", action="store_true", help="plug in UL 2017 sample")
parser.add_argument("--only2017", action="store_true", help="2017 EOY vs UL comparison")
parser.add_argument("--pt_cut", action="store_true", help="restrict to pt > 20 GeV")
parser.add_argument("--endcap", action="store_true", help="split in barrel and endcap")
parser.add_argument("--barrel", action="store_true", help="split in barrel and endcap")
parser.add_argument("--amcatnlo", action="store_true", help="split in barrel and endcap")
parser.add_argument("--pt", action="store_true", help="")
parser.add_argument("--ecal_driven", action="store_true", help="")
parser.add_argument("--tracker_driven", action="store_true", help="")

# parser.print_help()

args = parser.parse_args()

if args.only2017 and args.ul2017:
    raise RuntimeError("You can't set ul2017 and only2017 flags at the same time")

if args.barrel and args.endcap:
    args.barrel = False
    args.endcap = False

if args.ecal_driven and args.tracker_driven:
    args.ecal_driven = False
    args.tracker_driven = False

if args.amcatnlo and not args.only2017:
    raise RuntimeError("amcatnlo only available for only2017")

if (args.ecal_driven or args.tracker_driven) and not args.only2017:
    raise RuntimeError("ecal_driven or tracker_driven) only available for only2017")

###############
# Configuration
###############

root_file_name = {
    "2016": "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2016_DYJets.root",
    "2017": "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2017_DYJets.root",
    "2017ul": "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2017_DYJets_UL.root",
    "2018": "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2018_DYJets.root",
}

if args.amcatnlo:
    root_file_name["2017"] = "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2017_DYJets_amcatnloFXFX.root"
    root_file_name["2017ul"] = "~/data/ntuples/egamma_DPS_2017/electron_ntuple_2017_DYJets_amcatnloFXFX_UL.root"

sample_2017 = "2017ul" if args.ul2017 else "2017"
samples = ("2016", sample_2017, "2018")
if args.only2017:
    samples = ["2017", "2017ul"]

labels = {"2016": "2016", "2017": "2017", "2018": "2018", "2017ul": "2017 (Legacy)"}
colors = {"2016": 30, "2017": 38, "2018": 46, "2017ul": 38}
marker_styles = {"2016": 8, "2017": 8, "2018": 8, "2017ul": 4}
# colors = {"2016": 30, "2017": 38, "2018": 46, "2017ul": 36}
# marker_styles = {"2016": 8, "2017": 8, "2018": 8, "2017ul": 8}

bins = {"genNpu": (78, 2.0, 80.0), "ele_pt": (200, 5, 130)}

# Globals for getting the data

tdrstyle.set()

writeExtraText = True  # if extra text
# extraText  = "Preliminary"  # default extra text is "Preliminary"
extraText = ""

iPeriod = 3  # 1=7TeV, 2=8TeV, 3=7+8TeV, 7=7+8+13TeV, 0=free form (uses lumi_sqrtS)

W = 600
H = 600

W_ref = 600
H_ref = 600

# references for T, B, L, R
T = 0.08 * H_ref
B = 0.14 * H_ref
L = 0.18 * W_ref
R = 0.04 * W_ref


def setup_legend(legend):
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextAngle(0)
    legend.SetTextColor(ROOT.kBlack)
    # legend.SetTextSize(0.04)
    legend.SetTextSize(0.05)
    legend.SetTextAlign(12)


dummies = []


def add_legend_item(
    legend,
    label="",
    option="l",
    draw_opts="p",
    marker_size=1,
    marker_style=20,
    line_width=1,
    line_color=1,
    line_style=1,
    fill_color=1,
):
    random_name = "%032x" % random.getrandbits(128)
    dummies.append(ROOT.TGraph(1, np.zeros(1, dtype=np.float), np.zeros(1, dtype=np.float)))
    dummies[-1].SetName(random_name)
    dummies[-1].Draw(draw_opts)
    dummies[-1].SetMarkerStyle(marker_style)
    dummies[-1].SetLineWidth(line_width)
    dummies[-1].SetLineColor(line_color)
    dummies[-1].SetLineStyle(line_style)
    dummies[-1].SetFillColor(fill_color)
    dummies[-1].SetMarkerSize(marker_size)
    dummies[-1].Draw(draw_opts)
    legend.AddEntry(random_name, label, option)


##########
# Get Data
##########

root_file = dict()
root_dir = dict()
root_tree = dict()
root_tree_event = dict()

for sample in samples:
    print("opening", sample)
    root_file[sample] = ROOT.TFile(root_file_name[sample], "READ")
    root_dir[sample] = root_file[sample].Get("ntuplizer")
    root_tree[sample] = root_dir[sample].Get("tree")
    root_tree_event[sample] = root_dir[sample].Get("tree_event")

ROOT.gROOT.SetBatch(True)

event_npu = {}
bkg_hist = {}

variable = "genNpu"
if args.pt:
    variable = "ele_pt"

for sample in samples:
    event_npu[sample] = ROOT.TH1F("npu" + sample, "npu" + sample, *bins["genNpu"])
    bkg_hist[sample] = ROOT.TH1F("bkg" + sample, "bkg" + sample, *bins[variable])

    root_tree_event[sample].Draw("genNpu>>npu" + sample, "", "")

eta_selection = "abs(scl_eta) < 2.5 && (abs(scl_eta) < 1.444 || abs(scl_eta) >= 1.566)"

if args.barrel:
    eta_selection = eta_selection + " && abs(scl_eta) < 1.444"
if args.endcap:
    eta_selection = eta_selection + " && abs(scl_eta) >= 1.566"

base_selection = "matchedToGenEle != 1 && matchedToGenEle != 2 && " + eta_selection

if args.ecal_driven:
    base_selection = base_selection + " && ele_isEcalDriven"
if args.tracker_driven:
    base_selection = base_selection + " && ele_isTrackerDriven"

canv_name = "n_fakes_vs_" + variable

if args.pt_cut:
    # exclude gap
    for sample in samples:
        root_tree[sample].Draw(variable + ">>bkg" + sample, "ele_pt < 250. && ele_pt >= 20. && " + base_selection, "")

    canv_name = canv_name + "_pt20"
else:
    # exclude gap
    if not args.pt:
        for sample in samples:
            root_tree[sample].Draw(
                variable + ">>bkg" + sample, "ele_pt < 20.  && ele_pt >= 5. && " + base_selection, ""
            )

        canv_name = canv_name + "_pt5"

    else:
        for sample in samples:
            root_tree[sample].Draw(variable + ">>bkg" + sample, "ele_pt >= 5. && " + base_selection, "")

if args.ul2017:
    canv_name = canv_name + "_ul2017"
elif args.only2017:
    canv_name = canv_name + "_only2017"
if args.barrel:
    canv_name = canv_name + "_barrel"
elif args.endcap:
    canv_name = canv_name + "_endcap"

if args.ecal_driven:
    canv_name = canv_name + "_ecal_driven"
elif args.tracker_driven:
    canv_name = canv_name + "_tracker_driven"

if args.amcatnlo:
    canv_name = canv_name + "_amcatnlo"

for sample in samples:
    bkg_hist[sample].Sumw2()
    if args.pt:
        bkg_hist[sample].Scale(1.0 / root_tree_event[sample].GetEntries())
    else:
        bkg_hist[sample].Divide(event_npu[sample])

##########
# Plotting
##########

canv = ROOT.TCanvas(canv_name, canv_name, 50, 50, W, H)
canv.SetFillColor(0)
canv.SetBorderMode(0)
canv.SetFrameFillStyle(0)
canv.SetFrameBorderMode(0)
canv.SetLeftMargin(L / W)
canv.SetRightMargin(R / W)
canv.SetTopMargin(T / H)
canv.SetBottomMargin(B / H)
canv.SetTickx(0)
canv.SetTicky(0)

if args.pt:
    canv.SetLogx()
    canv.SetLogy()

canv.DrawFrame(0.0, 80.0, 0.0, 10.0)

for sample in samples:
    bkg_hist[sample].SetMarkerColor(colors[sample])
    bkg_hist[sample].SetMarkerStyle(marker_styles[sample])
    bkg_hist[sample].SetLineColor(colors[sample])

if args.pt:
    bkg_hist[samples[0]].GetXaxis().SetTitle("Electron p_{T} [GeV]")
else:
    bkg_hist[samples[0]].GetXaxis().SetTitle("Number of Pileup")

bkg_hist[samples[0]].GetXaxis().SetTitleOffset(1.1)
bkg_hist[samples[0]].GetYaxis().SetTitleOffset(1.3)
bkg_hist[samples[0]].GetYaxis().SetTitle("DY+Jets Fakes per Event")

if not args.pt:
    if args.pt_cut:
        bkg_hist[samples[0]].GetYaxis().SetRangeUser(0, 0.0585)
    else:
        bkg_hist[samples[0]].GetYaxis().SetRangeUser(0, 0.74)

draw_options = "E"
for sample in samples:
    bkg_hist[sample].Draw(draw_options)
    draw_options = draw_options + "SAME"

if args.pt:
    # legend = ROOT.TLegend(1.0 - 0.33, 0.70, 1 - 0.06, 0.88)
    legend = ROOT.TLegend(0.21, 1 - 0.80, 0.49, 1 - 0.62)
else:
    legend = ROOT.TLegend(0.21, 0.70, 0.49, 0.88)

setup_legend(legend)
for sample in samples:
    label = labels[sample]
    if args.barrel:
        label = label + " Barrel"
    if args.endcap:
        label = label + " Endcap"
    if args.tracker_driven:
        label = label + " tracker driven"
    if args.ecal_driven:
        label = label + " ecal driven"
    if args.amcatnlo:
        label = label + " NLO"
    legend.AddEntry("bkg" + sample, label, "pe")
legend.Draw()

# writing the lumi information and the CMS "logo"
CMS_lumi(canv, extraText="        Simulation", iPosX=0)
# CMS_lumi( canv, extraText="Simulation Preliminary", iPosX=11)

canv.Update()
canv.RedrawAxis()
canv.GetFrame().Draw()

canv.SetTicky()
canv.SetTickx()

label_pt = ROOT.TLatex()
label_pt.SetNDC()
label_pt.SetTextAngle(0)
label_pt.SetTextColor(ROOT.kBlack)
label_pt.SetTextFont(52)  # 42 for not bold
label_pt.SetTextSize(0.04)
# if pt_cut:
# label_pt.DrawLatex(0.01,0.02, "Electron p_{T} > 20 GeV")
# else:
# label_pt.DrawLatex(0.01,0.02, "Electron p_{T} > 5 GeV")

canv.Draw()

canv.SaveAs("plots/" + canv_name + ".eps", ".eps")
canv.SaveAs("plots/" + canv_name + ".pdf", ".pdf")
canv.SaveAs("plots/" + canv_name + ".root", ".root")
canv.SaveAs("plots/" + canv_name + ".png", ".png")
canv.SaveAs("plots/" + canv_name + ".C", ".C")
