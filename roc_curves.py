import ROOT
import tdrstyle
import numpy as np
from CMS_lumi import CMS_lumi
from root_numpy import tree2array
from sklearn import linear_model
from sklearn.metrics import roc_curve
import random
import argparse
import os

parser = argparse.ArgumentParser()

parser.add_argument("--fakes", help="the type of fakes", default="DYJets")
parser.add_argument("--year", type=int, help="year of simulation", default=2017)
parser.add_argument(
    "--reweight", action="store_true", help="With kinematic reweighting of the signal to the background"
)
parser.add_argument("--twostep", action="store_true", help="Include ROC curve for MVA without iso plus iso in sequence")
parser.add_argument("--mvawp", action="store_true", help="Indicate the MVA working points in the plot")
parser.add_argument(
    "--nocutwp", dest="cutwp", action="store_false", help="Indicate the cut based working points in the plot"
)
parser.add_argument("--xrange", type=float, nargs="*", help="range for x axis", default=[0.6, 1.01])
parser.add_argument("--yrange", type=float, nargs="*", help="range for y axis", default=[0.0, 0.25])
parser.add_argument(
    "--stop", type=int, help="how many electrons to read from each ntuple", default=3000000
)  # -1 for None
parser.add_argument(
    "--bkg_sel",
    help="background selection",
    default="matchedToGenEle == 0 || matchedToGenEle == 3 || matchedToGenEle == 4",
)
parser.add_argument("--sig_sel", help="signal selection", default="matchedToGenEle == 1")
parser.add_argument("--suffix", help="what to append to the plot file names", default="")
parser.add_argument("--sf", action="store_true", help="apply scale factors on the working points")
parser.add_argument("--lowpt", action="store_true", help="pt 5 to 20")
parser.add_argument("--ylabel", help="Label for the y-axis", default=None)
parser.add_argument("--leg_right", action="store_true", help="put the legend on the right side")

# parser.print_help()

args = parser.parse_args()

year = args.year
fakes = args.fakes

ntuple_dir = os.environ["NTUPLE_DIR"]

root_file_name = os.path.join(ntuple_dir, "electron_ntuple_{}_DYJets.root".format(year))
root_file_name_bkg = os.path.join(ntuple_dir, "electron_ntuple_{0}_{1}.root".format(year, fakes))
weight_file_name = "weights/electron_ntuple_{0}_DYJets_weights_from_electron_ntuple_{0}_{1}.root".format(year, fakes)

if args.sf:
    from scale_factors import *


# Globals for getting the data

if args.lowpt:
    cats = {
        "EB": ROOT.TCut("ele_pt < 20 && ele_pt >= 5. && abs(scl_eta) < 1.479 && genNpu > 1"),
        "EE": ROOT.TCut("ele_pt < 20 && ele_pt >= 5. && abs(scl_eta) >= 1.479 && abs(scl_eta) < 2.5 && genNpu > 1"),
    }
else:
    cats = {
        "EB": ROOT.TCut("ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) < 1.479 && genNpu > 1"),
        "EE": ROOT.TCut("ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) >= 1.479 && abs(scl_eta) < 2.5 && genNpu > 1"),
    }

branches = [
    "ele_pt",
    "scl_eta",
    "matchedToGenEle",
    "Fall17V1veto",
    "Fall17V1loose",
    "Fall17V1medium",
    "Fall17V1tight",
    "Fall17isoV1wp80",
    "Fall17isoV1wp90",
    "Fall17isoV1wpLoose",
    "Fall17noIsoV1wp80",
    "Fall17noIsoV1wp90",
    "Fall17noIsoV1wpLoose",
    "Fall17IsoV1Vals",
    "Fall17NoIsoV1Vals",
    "ele_pfCombRelIso2016",
    "ele_pfCombRelIso2017",
]

if args.stop == -1:
    stop = None
else:
    stop = args.stop

# Globals for plotting

ROOT.gROOT.SetBatch(True)

tdrstyle.set()

writeExtraText = True  # if extra text
extraText = "Preliminary"  # default extra text is "Preliminary"

iPeriod = 3  # 1=7TeV, 2=8TeV, 3=7+8TeV, 7=7+8+13TeV, 0=free form (uses lumi_sqrtS)

W = 800
H = 600

W_ref = 800
H_ref = 600

# references for T, B, L, R
T = 0.08 * H_ref
B = 0.12 * H_ref
L = 0.12 * W_ref
R = 0.04 * W_ref


def setup_legend(legend):
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextAngle(0)
    legend.SetTextColor(ROOT.kBlack)
    legend.SetTextSize(0.04)
    legend.SetTextAlign(12)


dummies = []


def add_legend_item(
    legend,
    label="",
    option="l",
    draw_opts="p",
    marker_size=1,
    marker_style=20,
    line_width=2,
    line_color=1,
    line_style=1,
    fill_color=1,
):
    random_name = "%032x" % random.getrandbits(128)
    dummies.append(ROOT.TGraph(1, np.zeros(1, dtype=np.float), np.zeros(1, dtype=np.float)))
    dummies[-1].SetName(random_name)
    dummies[-1].Draw(draw_opts)
    dummies[-1].SetMarkerStyle(marker_style)
    dummies[-1].SetLineWidth(line_width)
    dummies[-1].SetLineColor(line_color)
    dummies[-1].SetLineStyle(line_style)
    dummies[-1].SetFillColor(fill_color)
    dummies[-1].SetMarkerSize(marker_size)
    dummies[-1].Draw(draw_opts)
    legend.AddEntry(random_name, label, option)


weight_label = "weight_{0}_{1}".format(year, fakes)

##########
# Get Data
##########

root_file = ROOT.TFile(root_file_name, "READ")
root_dir = root_file.Get("ntuplizer")
root_tree = root_dir.Get("tree")
if args.reweight:
    root_file_weights = ROOT.TFile(weight_file_name, "READ")
    root_tree_weights = root_file_weights.Get("tree")
    root_tree.AddFriend(root_tree_weights)

root_file_bkg = ROOT.TFile(root_file_name_bkg, "READ")
root_dir_bkg = root_file_bkg.Get("ntuplizer")
root_tree_bkg = root_dir_bkg.Get("tree")

if year == 2017:
    wp_labels = [
        "Fall17V1medium",
        "Fall17V1loose",
        "Fall17V1tight",
        "Fall17V1veto",
        "Fall17isoV1wp80",
        "Fall17isoV1wp90",
        "Fall17noIsoV1wp80",
        "Fall17noIsoV1wp90",
    ]  # , "Fall17isoV1wpLoose", "Fall17noIsoV1wpLoose"]
    wp_styles = [20] * 4 + [29] * 2 + [30] * 2
    wp_sizes = [2] * 4 + [3] * 2 + [3] * 2
if year == 2016:
    wp_labels = [
        "Fall17V1medium",
        "Fall17V1loose",
        "Fall17V1tight",
        "Fall17V1veto",
        "Fall17noIsoV1wp80",
        "Fall17noIsoV1wp90",
    ]  # , "Fall17noIsoV1wpLoose"]
    wp_styles = [20] * 4 + [29] * 2
    wp_sizes = [2] * 4 + [3] * 2

wp_effs = {}
roc_effs = {}

for cat in cats:

    roc_effs[cat] = {}
    wp_effs[cat] = {}

    selection = cats[cat]

    sig = tree2array(
        root_tree,
        selection=str(selection + ROOT.TCut(args.sig_sel)),
        branches=branches + [weight_label] * args.reweight,
        stop=stop,
    )
    bkg = tree2array(root_tree_bkg, selection=str(selection + ROOT.TCut(args.bkg_sel)), branches=branches, stop=stop)

    iso_label = "ele_pfCombRelIso" + str(year)

    y_score = np.concatenate([sig["Fall17NoIsoV1Vals"], bkg["Fall17NoIsoV1Vals"]])
    y_true = np.concatenate([np.ones(len(sig), dtype=np.bool), np.zeros(len(bkg), dtype=np.bool)])

    if args.reweight:
        weights = np.concatenate([sig[weight_label], np.ones(len(bkg), dtype=np.float)])

    if args.reweight:
        roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1, sample_weight=weights)
    else:
        roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1)

    roc_effs[cat]["NoIso"] = roc_sig_eff, roc_bkg_eff

    if args.twostep:
        y_iso = np.concatenate([sig[iso_label], bkg[iso_label]])
        regr = linear_model.LogisticRegression()
        X = np.vstack([y_score, y_iso]).T
        regr.fit(X, y_true)
        y_score_iso = regr.predict_proba(X)[:, 1] * 2.0 - 1.0

        if args.reweight:
            roc_bkg_eff_iso, roc_sig_eff_iso, _ = roc_curve(y_true, y_score_iso, pos_label=1, sample_weight=weights)
        else:
            roc_bkg_eff_iso, roc_sig_eff_iso, _ = roc_curve(y_true, y_score_iso, pos_label=1)

        roc_effs[cat]["2step"] = roc_sig_eff_iso, roc_bkg_eff_iso

    if year == 2017:
        y_score = np.concatenate([sig["Fall17IsoV1Vals"], bkg["Fall17IsoV1Vals"]])

        if args.reweight:
            roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1, sample_weight=weights)
        else:
            roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1)

        roc_effs[cat]["Iso"] = roc_sig_eff, roc_bkg_eff

    def get_wp_id_eff(label, reweight=False, apply_sf=args.sf):
        bkg_eff = np.sum(bkg[label] > 0) * 1.0 / len(bkg)
        if reweight:
            sig_eff = np.sum(sig[weight_label][sig[label] > 0]) * 1.0 / np.sum(sig[weight_label])
        else:
            sig_eff = np.sum(sig[label] > 0) * 1.0 / len(sig)

        if apply_sf:
            sf = get_scale_factors(sig["scl_eta"], sig["ele_pt"], label)
            sig_eff = sig_eff * np.mean(sf)

        return np.array(sig_eff, dtype=np.float), np.array(bkg_eff, dtype=np.float)

    for l in wp_labels:
        wp_effs[cat][l] = get_wp_id_eff(l, reweight=args.reweight)


##########
# Plotting
##########

canv_name = "ElectronID_ROC_{0}_{1}".format(year, fakes)

if args.lowpt:
    canv_name = canv_name.replace("ROC", "ROC_lowpt")
if args.reweight:
    canv_name = canv_name + "_reweight"
if not args.cutwp:
    canv_name = canv_name + "_nocutwp"
if args.mvawp:
    canv_name = canv_name + "_mvawp"
if args.twostep:
    canv_name = canv_name + "_twostep"
if args.sf:
    canv_name = canv_name + "_sf"
if args.suffix != "":
    canv_name = canv_name + "_" + args.suffix

canv = ROOT.TCanvas(canv_name, canv_name, 50, 50, W, H)
canv.SetFillColor(0)
canv.SetBorderMode(0)
canv.SetFrameFillStyle(0)
canv.SetFrameBorderMode(0)
canv.SetLeftMargin(L / W)
canv.SetRightMargin(R / W)
canv.SetTopMargin(T / H)
canv.SetBottomMargin(B / H)
canv.SetTickx(0)
canv.SetTicky(0)

canv.DrawFrame(0.0, 0.0, 1.0, 1.0)

gr_roc = {}

locs = ["EB", "EE"]

for loc in locs:
    gr_roc[loc] = {}
    n = len(roc_effs[loc]["NoIso"][1])
    gr_roc[loc]["NoIso"] = ROOT.TGraph(n, roc_effs[loc]["NoIso"][0], roc_effs[loc]["NoIso"][1])
    if year == 2017:
        n = len(roc_effs[loc]["Iso"][1])
        gr_roc[loc]["Iso"] = ROOT.TGraph(n, roc_effs[loc]["Iso"][0], roc_effs[loc]["Iso"][1])
    if args.twostep:
        n = len(roc_effs[loc]["2step"][1])
        gr_roc[loc]["2step"] = ROOT.TGraph(n, roc_effs[loc]["2step"][0], roc_effs[loc]["2step"][1])

if year == 2017:
    roc_label = "Iso"
if year == 2016:
    roc_label = "NoIso"

if args.sf:
    gr_roc[locs[0]][roc_label].GetXaxis().SetTitle("Signal Efficiency (w/ SF)")
else:
    gr_roc[locs[0]][roc_label].GetXaxis().SetTitle("Signal Efficiency")
gr_roc[locs[0]][roc_label].GetYaxis().SetTitleOffset(1)

if args.ylabel is None:
    gr_roc[locs[0]][roc_label].GetYaxis().SetTitle("{} Background Efficiency".format(fakes.replace("Jets", " + Jets")))
else:
    gr_roc[locs[0]][roc_label].GetYaxis().SetTitle(args.ylabel)

gr_roc[locs[0]][roc_label].Draw("AL")
gr_roc[locs[0]][roc_label].GetXaxis().SetRangeUser(*args.xrange)
gr_roc[locs[0]][roc_label].GetYaxis().SetRangeUser(*args.yrange)
gr_roc[locs[0]][roc_label].Draw("AL")
if year == 2016 and args.fakes == "TTJets":
    gr_roc[locs[0]][roc_label].SetLineColor(0)
else:
    gr_roc[locs[0]][roc_label].SetLineColor(38)
gr_roc[locs[0]][roc_label].SetLineWidth(2)
gr_roc[locs[0]][roc_label].SetLineStyle(1)
if args.sf:
    gr_roc[locs[0]][roc_label].SetLineColor(0)

if not args.sf:
    for i, loc in enumerate(locs):
        # if year == 2017:
        # gr_roc[loc]["Iso"].SetLineColor(38 + 8 * i)
        # gr_roc[loc]["Iso"].SetLineWidth(2)
        # gr_roc[loc]["Iso"].Draw("L")
        if args.twostep:
            gr_roc[loc]["2step"].SetLineStyle(3)
            gr_roc[loc]["2step"].SetLineColor(38 + 8 * i)
            gr_roc[loc]["2step"].SetLineWidth(2)
            gr_roc[loc]["2step"].Draw("L")
        if i > 0:
            if year == 2016:
                if year == 2016 and args.fakes == "TTJets":
                    gr_roc[loc]["NoIso"].SetLineColor(0)
                else:
                    gr_roc[loc]["NoIso"].SetLineColor(38 + 8 * i)
                gr_roc[loc]["NoIso"].SetLineWidth(2)
                gr_roc[loc]["NoIso"].SetLineStyle(1)
                gr_roc[loc]["NoIso"].Draw("L")
            if year == 2017:
                gr_roc[loc]["Iso"].SetLineColor(38 + 8 * i)
                gr_roc[loc]["Iso"].SetLineWidth(2)
                gr_roc[loc]["Iso"].SetLineStyle(1)
                gr_roc[loc]["Iso"].Draw("L")

gr_cut = {}

for j, loc in enumerate(locs):
    gr_cut[loc] = {}
    for i, l in enumerate(wp_labels):
        gr_cut[loc][l] = ROOT.TGraph(1, wp_effs[loc][l][0], wp_effs[loc][l][1])
        # gr_cut[loc][l].SetMarkerColor(1)
        gr_cut[loc][l].SetMarkerColor(38 + 8 * j)
        gr_cut[loc][l].SetMarkerStyle(wp_styles[i])
        gr_cut[loc][l].SetMarkerSize(wp_sizes[i])
        if args.mvawp and i >= 4 or args.cutwp and i < 4:
            gr_cut[loc][l].Draw("P")

# writing the lumi information and the CMS "logo"
CMS_lumi(canv, extraText="        Simulation Preliminary", iPosX=0)

canv.Update()
canv.RedrawAxis()
canv.GetFrame().Draw()

canv.SetTicky()
canv.SetTickx()

if args.leg_right:
    legend = ROOT.TLegend(0.66, 0.55, 0.94, 0.88)
else:
    legend = ROOT.TLegend(0.15, 0.55, 0.43, 0.88)
legend.SetHeader(str(year) + " detector simulation")
legend.AddEntry(0, "", "")
setup_legend(legend)

roc_option = "l" * (not args.sf) + "p" * args.mvawp

if args.cutwp:
    add_legend_item(legend, label="Cut based", option="p", marker_size=2, marker_style=20)
if year == 2017:
    # add_legend_item(legend, label="MVA w/o iso", option=roc_option, marker_size=3, line_style=2, marker_style=30)
    # add_legend_item(legend, label="MVA w/ iso", option=roc_option, marker_size=3, marker_style=29)
    add_legend_item(legend, label="MVA", option=roc_option, marker_size=3, marker_style=29)
if year == 2016 and args.fakes == "DYJets":
    add_legend_item(legend, label="MVA", option=roc_option, marker_size=3, line_style=1, marker_style=29)
if args.twostep:
    add_legend_item(legend, label="MVA and iso", option="l", marker_size=3, marker_style=30, line_style=3)

legend.AddEntry(0, "", "")
add_legend_item(legend, label="ECAL Barrel", option="f", fill_color=38, line_color=38)
add_legend_item(legend, label="ECAL Endcap", option="f", fill_color=46, line_color=46)

legend.Draw()

# if args.reweight:
# label_reweight = ROOT.TText()
# label_reweight.SetNDC()
# label_reweight.SetTextAngle(0)
# label_reweight.SetTextColor(ROOT.kBlack)
# label_reweight.SetTextFont(52) # 42 for not bold
# label_reweight.SetTextSize(0.03)
# label_reweight.DrawText(0.01,0.01, "Signal kinematically reweighted")

label_pt = ROOT.TLatex()
label_pt.SetNDC()
label_pt.SetTextAngle(0)
label_pt.SetTextColor(ROOT.kBlack)
label_pt.SetTextFont(52)  # 42 for not bold
label_pt.SetTextSize(0.04)
if args.lowpt:
    label_pt.DrawLatex(0.01, 0.02, "Electron p_{T} < 20 GeV")
else:
    label_pt.DrawLatex(0.01, 0.02, "Electron p_{T} > 20 GeV")


canv.Draw()

canv.SaveAs("plots/" + canv_name + ".eps", ".eps")
canv.SaveAs("plots/" + canv_name + ".png", ".png")
canv.SaveAs("plots/" + canv_name + ".root", ".root")
