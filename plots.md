# 2016 Plots
\clearpage

![ROC curves for the electron multivariate identification (Boosted Decision Trees) and the cut‐based selection working points. Signal from Drell-Yan+Jets Monte Carlo. Background are reconstructed electrons from Drell-Yan+Jets Monte Carlo which don't match a generated electron within a cone of size $Delta R = 0.1$. Electron candidates with $p_T > 20$ GeV and $|\eta| < 2.5$ are used.](./plots/ElectronID_ROC_2016_DYJets.pdf){ width=70% }

![ROC curves for the electron multivariate identification (Boosted Decision Trees) for low-$p_T$ electrons. Signal from Drell-Yan+Jets Monte Carlo. Background are reconstructed electrons from Drell-Yan+Jets Monte Carlo which don't match a generated electron within a cone of size $Delta R = 0.1$. Electron candidates with $5 < p_T < 20$ GeV and $|\eta| < 2.5$ are used.](./plots/ElectronID_ROC_lowpt_2016_DYJets_nocutwp.pdf){ width=70% }

\newpage
\clearpage
# 2017 Plots
\clearpage

![ROC curves for the electron multivariate identification (Boosted Decision Trees) and the cut‐based selection working points. Signal from Drell-Yan+Jets Monte Carlo. Background are reconstructed electrons from Drell-Yan+Jets Monte Carlo which don't match a generated electron within a cone of size $Delta R = 0.1$. Electron candidates with $p_T > 20$ GeV and $|\eta| < 2.5$ are used. The signal efficiencies are not corrected for data/MC scale factors, which affect more the MVA selection.](./plots/ElectronID_ROC_2017_DYJets.pdf){ width=70% }

![ROC curves for the electron multivariate identification (Boosted Decision Trees) and the cut‐based selection working points for signal from Drell-Yan+Jets Monte Carlo and non-prompt electrons from $t\bar{t}$+Jets Monte Carlo as background. Electron candidates with $p_T > 20$ GeV and $|\eta| < 2.5$ are used. The MVA selection (which has different use cases) was not trained for non-prompt background, thus performing slightly worse.](./plots/ElectronID_ROC_2017_TTJets_non_prompt.pdf){ width=70% }

![ROC curves for the electron multivariate identification (Boosted Decision Trees) for low-$p_T$ electrons. Signal from Drell-Yan+Jets Monte Carlo. Background are reconstructed electrons from Drell-Yan+Jets Monte Carlo which don't match a generated electron within a cone of size $Delta R = 0.1$. Electron candidates with $5 < p_T < 20$ GeV and $|\eta| < 2.5$ are used.](./plots/ElectronID_ROC_lowpt_2017_DYJets_nocutwp.pdf){ width=70% }

![ROC curves of the electron multivariate identification (Boosted Decision Trees) with and without the neutral, charged and hadron isolation. This is compared to a sequential selection of the MVA without the isolations plus a cut on the combined isolation. Especially for high efficiencies, the MVA with included isolations shows a clear advantage over the sequential approach. Electron candidates with $p_T > 20$ GeV and $|\eta| < 2.5$ are used.](./plots/ElectronID_ROC_2017_DYJets_sequential_iso.pdf){ width=70% }

\clearpage

![The number of unmatched reconstructed electron candidates (i.e before the ID step) with $p_T > 20$ GeV and $|\eta| < 2.5$ in 2016 and 2017 DY+Jets MC versus the true number of pileup. The suppression in 2017 is attributed to the new pixel detector. Note that the fake rate gets further reduced by the identification step which is not applied here.](./plots/n_fakes_pt20.pdf){ width=70% }

![The number of unmatched reconstructed electron candidates (i.e before the ID step) with $5 < p_T < 20$ GeV and $|\eta| < 2.5$ in 2016 and 2017 DY+Jets MC versus the true number of pileup. The suppression in 2017 is attributed to the new pixel detector. Note that the fake rate gets further reduced by the identification step which is not applied here.](./plots/n_fakes_pt5.pdf){ width=70% }
