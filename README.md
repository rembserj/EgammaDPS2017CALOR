# DPS_2017
A few scripts I wrote to to plots for the e/gamma detector performance summary 2017 and later adapted to update the plots for the 2020 EGM paper.

## Samples

The samples that should be used to make these performence plots are:

* `/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6_ext1-v2/MINIAODSIM`
* `/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17MiniAODv2-PU2017RECOSIMstep_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM`
* `/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIAutumn18MiniAOD-102X_upgrade2018_realistic_v15-v1/MINIAODSIM`

For the most recent fake-rate vs. PU plot, the 2017 sample has been update to MiniAODv2:

* `/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIIFall17MiniAODv2-PU2017RECOSIMstep_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM`

For the 2017 "ulta legacy" campaign, the following samples have been processed too:

* `/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer19UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM`
* `/DYJetsToEE_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer19UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM`
* `/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer19UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM`

The ntuplizer plugin and configuration can be found in the files [ElectronNtuplizer.cc](https://gitlab.cern.ch/rembserj/EgammaDPS2017CALOR/-/blob/master/ElectronNtuplizer.cc) and [ElectronNtuplizer_cfg.py](https://gitlab.cern.ch/rembserj/EgammaDPS2017CALOR/-/blob/master/ElectronNtuplizer_cfg.py).

Set up the direction where the ntuples are saved:

export NTUPLE_DIR=/eos/user/r/rembserj/ntuples/egamma_DPS_2017

Or on my Laptop:

export NTUPLE_DIR=/eos/user/r/rembserj/ntuples/egamma_DPS_2017

## Reweighting

./reweighting.sh

## Making Plots

./make_plots.sh

To get the sequential isolation plot like in the EGM Run 2 paper, please run:
```
python sequential_iso.py --fakes DYJets --stop -1
```
To get the number of fakes plot, also appearing in nthe paper:
```
python number_of_fakes.py
```
