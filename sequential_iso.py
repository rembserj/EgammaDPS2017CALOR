import ROOT
import tdrstyle
import numpy as np
from CMS_lumi import CMS_lumi
from root_numpy import tree2array
from sklearn import linear_model
from sklearn.metrics import roc_curve
import random
import argparse
import os

parser = argparse.ArgumentParser()

parser.add_argument("--fakes", help="the type of fakes", default="DYJets")
parser.add_argument("--year", type=int, help="year of simulation", default=2017)
parser.add_argument(
    "--reweight", action="store_true", help="With kinematic reweighting of the signal to the background"
)
parser.add_argument("--mvawp", action="store_true", help="Indicate the MVA working points in the plot")
parser.add_argument("--xrange", type=float, nargs="*", help="range for x axis", default=[0.61, 1.01])
parser.add_argument("--yrange", type=float, nargs="*", help="range for y axis", default=[0.0, 0.082])
parser.add_argument(
    "--stop", type=int, help="how many electrons to read from each ntuple", default=3000000
)  # -1 for None
parser.add_argument(
    "--bkg_sel",
    help="background selection",
    default="matchedToGenEle == 0 || matchedToGenEle == 3 || matchedToGenEle == 4",
)
parser.add_argument("--sig_sel", help="signal selection", default="matchedToGenEle == 1")
parser.add_argument("--suffix", help="what to append to the plot file names", default="")
parser.add_argument("--ylabel", help="Label for the y-axis", default=None)
parser.add_argument("--leg_right", action="store_true", help="put the legend on the right side")


# parser.print_help()

args = parser.parse_args()

year = args.year
fakes = args.fakes

ntuple_dir = os.environ["NTUPLE_DIR"]

root_file_name = os.path.join(ntuple_dir, "electron_ntuple_{}_DYJets_MiniAODv1.root".format(year))
root_file_name_bkg = os.path.join(ntuple_dir, "electron_ntuple_{0}_{1}_MiniAODv1.root".format(year, fakes))
weight_file_name = "weights/electron_ntuple_{0}_DYJets_weights_from_electron_ntuple_{0}_{1}.root".format(year, fakes)

# Globals for getting the data

cats = {
    "EB": ROOT.TCut("ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) < 1.479 && genNpu > 1"),
    "EE": ROOT.TCut("ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) >= 1.479 && abs(scl_eta) < 2.5 && genNpu > 1"),
}

branches = [
    "ele_pt",
    "scl_eta",
    "matchedToGenEle",
    "Fall17V1veto",
    "Fall17V1loose",
    "Fall17V1medium",
    "Fall17V1tight",
    "Fall17isoV1wp80",
    "Fall17isoV1wp90",
    "Fall17isoV1wpLoose",
    "Fall17noIsoV1wp80",
    "Fall17noIsoV1wp90",
    "Fall17noIsoV1wpLoose",
    "Fall17IsoV1Vals",
    "Fall17NoIsoV1Vals",
    "ele_pfCombRelIso2016",
    "ele_pfCombRelIso2017",
]

if args.stop == -1:
    stop = None
else:
    stop = args.stop

# Globals for plotting

ROOT.gROOT.SetBatch(True)

tdrstyle.set()

writeExtraText = True  # if extra text
extraText = "Preliminary"  # default extra text is "Preliminary"

iPeriod = 99  # 1=7TeV, 2=8TeV, 3=7+8TeV, 7=7+8+13TeV, 0=free form (uses lumi_sqrtS)


def setup_legend(legend):
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextAngle(0)
    legend.SetTextColor(ROOT.kBlack)
    legend.SetTextSize(0.04)
    legend.SetTextAlign(12)


dummies = []


def add_legend_item(
    legend,
    label="",
    option="l",
    draw_opts="p",
    marker_size=1,
    marker_style=20,
    line_width=2,
    line_color=1,
    line_style=1,
    fill_color=1,
):
    random_name = "%032x" % random.getrandbits(128)
    dummies.append(ROOT.TGraph(1, np.zeros(1, dtype=np.float), np.zeros(1, dtype=np.float)))
    dummies[-1].SetName(random_name)
    dummies[-1].Draw(draw_opts)
    dummies[-1].SetMarkerStyle(marker_style)
    dummies[-1].SetLineWidth(line_width)
    dummies[-1].SetLineColor(line_color)
    dummies[-1].SetLineStyle(line_style)
    dummies[-1].SetFillColor(fill_color)
    dummies[-1].SetMarkerSize(marker_size)
    dummies[-1].Draw(draw_opts)
    legend.AddEntry(random_name, label, option)


weight_label = "weight_{0}_{1}".format(year, fakes)

##########
# Get Data
##########

root_file = ROOT.TFile(root_file_name, "READ")
root_dir = root_file.Get("ntuplizer")
root_tree = root_dir.Get("tree")
if args.reweight:
    root_file_weights = ROOT.TFile(weight_file_name, "READ")
    root_tree_weights = root_file_weights.Get("tree")
    root_tree.AddFriend(root_tree_weights)

root_file_bkg = ROOT.TFile(root_file_name_bkg, "READ")
root_dir_bkg = root_file_bkg.Get("ntuplizer")
root_tree_bkg = root_dir_bkg.Get("tree")

roc_effs = {}

for cat in cats:

    roc_effs[cat] = {}

    selection = cats[cat]

    sig = tree2array(
        root_tree,
        selection=str(selection + ROOT.TCut(args.sig_sel)),
        branches=branches + [weight_label] * args.reweight,
        stop=stop,
    )
    bkg = tree2array(root_tree_bkg, selection=str(selection + ROOT.TCut(args.bkg_sel)), branches=branches, stop=stop)

    iso_label = "ele_pfCombRelIso" + str(year)

    y_score = np.concatenate([sig["Fall17NoIsoV1Vals"], bkg["Fall17NoIsoV1Vals"]])
    y_true = np.concatenate([np.ones(len(sig), dtype=np.bool), np.zeros(len(bkg), dtype=np.bool)])

    if args.reweight:
        weights = np.concatenate([sig[weight_label], np.ones(len(bkg), dtype=np.float)])

    if args.reweight:
        roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1, sample_weight=weights)
    else:
        roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1)

    roc_effs[cat]["NoIso"] = roc_sig_eff, roc_bkg_eff

    y_iso = np.concatenate([sig[iso_label], bkg[iso_label]])
    regr = linear_model.LogisticRegression()
    X = np.vstack([y_score, y_iso]).T
    regr.fit(X, y_true)
    y_score_iso = regr.predict_proba(X)[:, 1] * 2.0 - 1.0

    if args.reweight:
        roc_bkg_eff_iso, roc_sig_eff_iso, _ = roc_curve(y_true, y_score_iso, pos_label=1, sample_weight=weights)
    else:
        roc_bkg_eff_iso, roc_sig_eff_iso, _ = roc_curve(y_true, y_score_iso, pos_label=1)

    roc_effs[cat]["2step"] = roc_sig_eff_iso, roc_bkg_eff_iso

    if year == 2017:
        y_score = np.concatenate([sig["Fall17IsoV1Vals"], bkg["Fall17IsoV1Vals"]])

        if args.reweight:
            roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1, sample_weight=weights)
        else:
            roc_bkg_eff, roc_sig_eff, _ = roc_curve(y_true, y_score, pos_label=1)

        roc_effs[cat]["Iso"] = roc_sig_eff, roc_bkg_eff


##########
# Plotting
##########

canv_name = "ElectronID_ROC_{0}_{1}_sequential_iso".format(year, fakes)

# W = 600
# H = 600
W = 812
H = 800

# W_ref = 600
# H_ref = 600
W_ref = 812
H_ref = 800

# references for T, B, L, R
T = 0.08 * H_ref
B = 0.14 * H_ref
L = 0.18 * W_ref
R = 0.04 * W_ref

canv = ROOT.TCanvas(canv_name, canv_name, 50, 50, W, H)
# canv.SetFillColor(0)
# canv.SetBorderMode(0)
# canv.SetFrameFillStyle(0)
# canv.SetFrameBorderMode(0)
# canv.SetLeftMargin(L / W)
# canv.SetRightMargin(R / W)
# canv.SetTopMargin(T / H)
# canv.SetBottomMargin(B / H)
# canv.SetTickx(0)
# canv.SetTicky(0)

canv.Range(0.5396575,-0.06,1.022079,0.44)
canv.SetFillColor(0)
canv.SetBorderMode(0)
canv.SetBorderSize(2)
canv.SetTickx(1)
canv.SetTicky(1)
canv.SetLeftMargin(0.15)
canv.SetRightMargin(0.03)
canv.SetTopMargin(0.055)
canv.SetBottomMargin(0.13)
canv.SetFrameFillStyle(0)
canv.SetFrameBorderMode(0)
canv.SetFrameFillStyle(0)
canv.SetFrameBorderMode(0)

canv.DrawFrame(0.0, 0.0, 1.0, 1.0)

gr_roc = {}

locs = ["EB", "EE"]

for loc in locs:
    gr_roc[loc] = {}
    n = len(roc_effs[loc]["NoIso"][1])
    gr_roc[loc]["NoIso"] = ROOT.TGraph(n, roc_effs[loc]["NoIso"][0], roc_effs[loc]["NoIso"][1])
    if year == 2017:
        n = len(roc_effs[loc]["Iso"][1])
        gr_roc[loc]["Iso"] = ROOT.TGraph(n, roc_effs[loc]["Iso"][0], roc_effs[loc]["Iso"][1])
    n = len(roc_effs[loc]["2step"][1])
    gr_roc[loc]["2step"] = ROOT.TGraph(n, roc_effs[loc]["2step"][0], roc_effs[loc]["2step"][1])

gr_roc[locs[0]]["Iso"].GetXaxis().SetTitle("Signal efficiency")
gr_roc[locs[0]]["Iso"].GetXaxis().SetTitleOffset(1.1)
# gr_roc[locs[0]]["Iso"].GetYaxis().SetTitleOffset(1.4)
gr_roc[locs[0]]["Iso"].GetYaxis().SetTitleOffset(1.55)
gr_roc[locs[0]]["Iso"].GetXaxis().SetLabelSize(0.043)
gr_roc[locs[0]]["Iso"].GetYaxis().SetLabelSize(0.043)

gr_roc[locs[0]]["Iso"].GetXaxis().SetTitleSize(0.05)
gr_roc[locs[0]]["Iso"].GetYaxis().SetTitleSize(0.05)
gr_roc[locs[0]]["Iso"].GetXaxis().SetLabelSize(0.043)
gr_roc[locs[0]]["Iso"].GetYaxis().SetLabelSize(0.043)
gr_roc[locs[0]]["Iso"].GetYaxis().SetLabelOffset(0.007)

# Graph_Graph01->GetXaxis()->SetTitleFont(42);
# Graph_Graph01->GetXaxis()->SetTitleSize(0.05);//(0.9*Graph_Graph01->GetXaxis()->GetTitleSize());
# Graph_Graph01->GetXaxis()->SetLabelSize(0.043);
# Graph_Graph01->GetYaxis()->SetTitleSize(0.05);//0.9*Graph_Graph01->GetYaxis()->GetTitleSize());
# Graph_Graph01->GetYaxis()->SetLabelSize(0.043);
# Graph_Graph01->GetYaxis()->SetLabelOffset(0.007);
# Graph_Graph01->GetYaxis()->SetTitleOffset(1.55);
# Graph_Graph01->GetYaxis()->SetTitle("Background efficiency");

if args.ylabel is None:
    gr_roc[locs[0]]["Iso"].GetYaxis().SetTitle("{} background efficiency".format(fakes.replace("Jets", " + jets")))
else:
    gr_roc[locs[0]]["Iso"].GetYaxis().SetTitle(args.ylabel)

gr_roc[locs[0]]["Iso"].Draw("AL")
gr_roc[locs[0]]["Iso"].GetXaxis().SetRangeUser(*args.xrange)
gr_roc[locs[0]]["Iso"].GetYaxis().SetRangeUser(*args.yrange)
gr_roc[locs[0]]["Iso"].Draw("AL")
gr_roc[locs[0]]["Iso"].SetLineColor(46)
gr_roc[locs[0]]["Iso"].SetLineWidth(2)
gr_roc[locs[0]]["Iso"].SetLineStyle(1)

for i, loc in enumerate(locs):
    gr_roc[loc]["NoIso"].SetLineColor(30)
    gr_roc[loc]["NoIso"].SetLineWidth(2)
    gr_roc[loc]["NoIso"].SetLineStyle(1 + i)
    gr_roc[loc]["NoIso"].Draw("L")
    gr_roc[loc]["2step"].SetLineStyle(1 + i)
    gr_roc[loc]["2step"].SetLineColor(38)
    gr_roc[loc]["2step"].SetLineWidth(2)
    gr_roc[loc]["2step"].Draw("L")
    if i > 0:
        gr_roc[loc]["Iso"].SetLineColor(46)
        gr_roc[loc]["Iso"].SetLineWidth(2)
        gr_roc[loc]["Iso"].SetLineStyle(1 + i)
        gr_roc[loc]["Iso"].Draw("L")

# writing the lumi information and the CMS "logo"
# CMS_lumi(canv, extraText="        Simulation", iPosX=0, iPeriod=iPeriod)
# CMS_lumi( canv, extraText="Simulation Preliminary", iPosX=11)

tex1 = ROOT.TLatex(0.97,0.956,"(13 TeV) 2017")
tex1.SetNDC()
tex1.SetTextAlign(31)
tex1.SetTextFont(42)
tex1.SetTextSize(0.04)
tex1.SetLineWidth(2)
tex1.Draw()
tex2 = ROOT.TLatex(0.15,0.953,"CMS")
tex2.SetNDC()
tex2.SetTextFont(61)
tex2.SetTextSize(0.05)
tex2.SetLineWidth(2)
tex2.Draw()
tex3 = ROOT.TLatex(0.26,0.955,"Simulation")
tex3.SetNDC()
tex3.SetTextFont(52)
tex3.SetTextSize(0.04)
tex3.SetLineWidth(2)
tex3.Draw()

canv.Update()
canv.RedrawAxis()
canv.GetFrame().Draw()

canv.SetTicky()
canv.SetTickx()

if args.leg_right:
    legend = ROOT.TLegend(0.66, 0.55, 0.94, 0.88)
else:
    legend = ROOT.TLegend(0.21, 0.55, 0.49, 0.88)
# legend.SetHeader(str(year))
# legend.AddEntry(0, "", "")
setup_legend(legend)

roc_option = "l"

add_legend_item(legend, label="BDT w/o iso", option="f", line_color=30, fill_color=30)
add_legend_item(legend, label="BDT and iso", option="f", line_color=38, fill_color=38)
add_legend_item(legend, label="BDT w/ iso", option="f", line_color=46, fill_color=46)
legend.AddEntry(0, "", "")
add_legend_item(legend, label="ECAL Barrel", option="l", fill_color=1, line_color=1)
add_legend_item(legend, label="ECAL Endcap", option="l", fill_color=1, line_color=1, line_style=2)

legend.SetBorderSize(0)
legend.SetTextFont(42)
legend.SetLineColor(1)
legend.SetLineStyle(1)
legend.SetLineWidth(1)
legend.SetFillColor(0)
legend.SetFillStyle(0)
legend.SetTextSize(0.045)

legend.Draw()

if args.reweight:
    label_reweight = ROOT.TText()
    label_reweight.SetNDC()
    label_reweight.SetTextAngle(0)
    label_reweight.SetTextColor(ROOT.kBlack)
    label_reweight.SetTextFont(52)  # 42 for not bold
    label_reweight.SetTextSize(0.03)
    label_reweight.DrawText(0.01, 0.01, "Signal kinematically reweighted")

canv.Draw()

canv.SaveAs("plots/" + canv_name + ".eps", ".eps")
canv.SaveAs("plots/" + canv_name + ".pdf", ".pdf")
canv.SaveAs("plots/" + canv_name + ".png", ".png")
canv.SaveAs("plots/" + canv_name + ".root", ".root")
canv.SaveAs("plots/" + canv_name + ".C", ".C")
