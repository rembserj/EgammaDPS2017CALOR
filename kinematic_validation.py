import ROOT
import numpy as np
from root_numpy import tree2array
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter

ROOT.gROOT.SetBatch(True)

stop = None

root_file = ROOT.TFile("./ntuples/pt_ntuple_2017_DYJets.root", "READ")
root_dir = root_file.Get("ntuplizer")
root_tree = root_dir.Get("tree")
root_tree_meta = root_dir.Get("tree_meta")

root_file_2016 = ROOT.TFile("./ntuples/pt_ntuple_2016_DYJets.root", "READ")
root_dir_2016 = root_file_2016.Get("ntuplizer")
root_tree_2016 = root_dir_2016.Get("tree")
root_tree_meta_2016 = root_dir_2016.Get("tree_meta")

n_events = np.sum(tree2array(root_tree_meta)["nEvents"])
n_events_2016 = np.sum(tree2array(root_tree_meta_2016)["nEvents"])

arr = tree2array(root_tree, stop=None)
pt, eta = arr["pt"], arr["eta"]
pt = pt[np.abs(eta) < 2.5]
eta = eta[arr["pt"] > 1]
v = {"pt": pt, "eta": eta}

arr_2016 = tree2array(root_tree_2016, stop=None)
pt_2016, eta_2016 = arr_2016["pt"], arr_2016["eta"]
pt_2016 = pt_2016[np.abs(eta_2016) < 2.5]
eta_2016 = eta_2016[arr_2016["pt"] > 1]
v_2016 = {"pt": pt_2016, "eta": eta_2016}

# ptedges = np.array(list(range(0, 51))[::1] + [55.0,60.0,65.0,70.0,75.0,80.0,90.0,100.0,150.0,200.0,250.0])
ptedges = np.linspace(0, 2.25, 50)
etaedges = np.linspace(-2.5, 2.5, 50)


def create_axes(yunits=4):
    # fig = plt.figure(figsize=(6.4, 4.6)) # the default figsize
    fig = plt.figure(figsize=(6.4, 4.8))  # the default figsize
    gs = gridspec.GridSpec(yunits, 1)
    ax1 = plt.subplot(gs[:2, :])
    ax2 = plt.subplot(gs[2:, :])
    axarr = [ax1, ax2]

    gs.update(wspace=0.025, hspace=0.075)

    plt.setp(ax1.get_xticklabels(), visible=False)

    # ax1.grid()
    # ax2.grid()

    return ax1, ax2


for vname, edges in zip(["pt", "eta"], [ptedges, etaedges]):

    if vname == "pt":
        bin_centers = 10 ** (edges[:-1]) + np.diff(10 ** edges) / 2.0
        hist_2016, _, _ = plt.hist(
            np.log10(v_2016[vname]), edges, histtype="step", label="2016", density=False, log=True
        )
        hist, _, _ = plt.hist(np.log10(v[vname]), edges, histtype="step", label="2017", density=False, log=True)
    else:
        bin_centers = edges[:-1] + np.diff(edges) / 2.0
        hist_2016, _, _ = plt.hist(v_2016[vname], edges, histtype="step", label="2016", density=False)
        hist, _, _ = plt.hist(v[vname], edges, histtype="step", label="2017", density=False)

    hist_2016_err = np.sqrt(hist_2016) / n_events_2016
    hist_err = np.sqrt(hist) / n_events
    hist_2016 = hist_2016 / n_events_2016
    hist = hist / n_events
    plt.close()

    ax1, ax2 = create_axes(yunits=3)

    if vname == "pt":
        ax1.semilogx([], [], "k")
        ax2.semilogx([], [], "k")

    ax1.step(bin_centers, hist_2016, label="2016")
    ax1.step(bin_centers, hist, label="2017")

    ax1.fill_between(bin_centers, hist_2016 - hist_2016_err, hist_2016 + hist_2016_err, alpha=0.5, step="pre")
    ax1.fill_between(bin_centers, hist - hist_err, hist + hist_err, alpha=0.5, step="pre")
    ax1.legend()
    ax1.set_ylim(0, ax1.get_ylim()[1])

    ax2.step(bin_centers, [1] * len(bin_centers))
    ax2.step(bin_centers, hist / hist_2016)
    ax2.fill_between(
        bin_centers,
        (hist_2016 - hist_2016_err) / hist_2016,
        (hist_2016 + hist_2016_err) / hist_2016,
        alpha=0.5,
        step="pre",
    )
    ax2.fill_between(bin_centers, (hist - hist_err) / hist_2016, (hist + hist_err) / hist_2016, alpha=0.5, step="pre")

    ax2.set_ylabel("Ratio")
    ax2.set_xlabel(vname)
    ax1.set_ylabel("Particles per Event")

    plt.savefig("2016_2017_{}.png".format(vname))
    plt.savefig("2016_2017_{}.pdf".format(vname))
