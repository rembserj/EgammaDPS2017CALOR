import ROOT
import numpy as np
from root_numpy import tree2array
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter

ROOT.gROOT.SetBatch(True)

stop = None

root_file_name_2016 = "ntuples/electron_ntuple_2016_DYJets_w_jets.root"
root_file_name = "ntuples/electron_ntuple_2017_DYJets_w_jets.root"

root_file_2016 = ROOT.TFile(root_file_name_2016, "READ")
root_dir_2016 = root_file_2016.Get("ntupler")
root_tree_2016 = root_dir_2016.Get("GenJetTree")
root_tree_event_2016 = root_dir_2016.Get("EventTree")

root_file = ROOT.TFile(root_file_name, "READ")
root_dir = root_file.Get("ntupler")
root_tree = root_dir.Get("GenJetTree")
root_tree_event = root_dir.Get("EventTree")

n_events = root_tree_event.GetEntries("genNpu == 35")
n_events_2016 = root_tree_event_2016.GetEntries("genNpu == 35")

evtnums = tree2array(root_tree_event, branches=["evtnum"], selection="genNpu == 35")["evtnum"]
evtnums_2016 = tree2array(root_tree_event_2016, branches=["evtnum"], selection="genNpu == 35")["evtnum"]

branches = ["evtnum", "genJetEta", "genJetPt"]
arr = tree2array(root_tree, branches=branches, selection="matchedToGenEle == 0  && abs(genJetEta < 2.5)", stop=None)
arr_2016 = tree2array(
    root_tree_2016, branches=branches, selection="matchedToGenEle == 0  && abs(genJetEta < 2.5)", stop=None
)

arr = arr[np.in1d(arr["evtnum"], evtnums)]
arr_2016 = arr_2016[np.in1d(arr_2016["evtnum"], evtnums_2016)]

# ptedges = np.array(list(range(0, 51))[::1] + [55.0,60.0,65.0,70.0,75.0,80.0,90.0,100.0,150.0,200.0,250.0])
# ptedges = np.linspace(0, 2.25, 50)
ptedges = np.linspace(0.68, 2.25, 50)
etaedges = np.linspace(-2.5, 2.5, 50)


def create_axes(yunits=4):
    # fig = plt.figure(figsize=(6.4, 4.6)) # the default figsize
    fig = plt.figure(figsize=(6.4, 4.8))  # the default figsize
    gs = gridspec.GridSpec(yunits, 1)
    ax1 = plt.subplot(gs[:2, :])
    ax2 = plt.subplot(gs[2:, :])
    axarr = [ax1, ax2]

    gs.update(wspace=0.025, hspace=0.075)

    plt.setp(ax1.get_xticklabels(), visible=False)

    # ax1.grid()
    # ax2.grid()

    return ax1, ax2


for vname, edges in zip(["genJetPt", "genJetEta"], [ptedges, etaedges]):

    if vname == "genJetPt":
        bin_centers = 10 ** (edges[:-1]) + np.diff(10 ** edges) / 2.0
        hist_2016, _, _ = plt.hist(
            np.log10(arr_2016[vname]), edges, histtype="step", label="2016", density=False, log=True
        )
        hist, _, _ = plt.hist(np.log10(arr[vname]), edges, histtype="step", label="2017", density=False, log=True)
    else:
        bin_centers = edges[:-1] + np.diff(edges) / 2.0
        hist_2016, _, _ = plt.hist(arr_2016[vname], edges, histtype="step", label="2016", density=False)
        hist, _, _ = plt.hist(arr[vname], edges, histtype="step", label="2017", density=False)

    hist_2016_err = np.sqrt(hist_2016) / n_events_2016
    hist_err = np.sqrt(hist) / n_events
    hist_2016 = hist_2016 / n_events_2016
    hist = hist / n_events
    plt.close()

    ax1, ax2 = create_axes(yunits=3)

    if vname == "genJetPt":
        ax1.semilogx([], [], "k")
        ax2.semilogx([], [], "k")

    ax1.step(bin_centers, hist_2016, label="2016")
    ax1.step(bin_centers, hist, label="2017")

    ax1.fill_between(bin_centers, hist_2016 - hist_2016_err, hist_2016 + hist_2016_err, alpha=0.5, step="pre")
    ax1.fill_between(bin_centers, hist - hist_err, hist + hist_err, alpha=0.5, step="pre")
    ax1.legend()
    ax1.set_ylim(0, ax1.get_ylim()[1])

    ax2.step(bin_centers, [1] * len(bin_centers))
    ax2.step(bin_centers, hist / hist_2016)
    ax2.fill_between(
        bin_centers,
        (hist_2016 - hist_2016_err) / hist_2016,
        (hist_2016 + hist_2016_err) / hist_2016,
        alpha=0.5,
        step="pre",
    )
    ax2.fill_between(bin_centers, (hist - hist_err) / hist_2016, (hist + hist_err) / hist_2016, alpha=0.5, step="pre")

    ax2.set_ylabel("Ratio")
    ax2.set_xlabel(vname)
    ax1.set_ylabel("Gen Jets per Event")

    plt.savefig("plots/kinematic_validation/gen_jets_PU35/2016_2017_{}.png".format(vname))
    plt.savefig("plots/kinematic_validation/gen_jets_PU35/2016_2017_{}.pdf".format(vname))
