#!/bin/bash


./create_weights.py --bkg_file $NTUPLE_DIR/electron_ntuple_2017_TTJets.root --sig_file $NTUPLE_DIR/electron_ntuple_2017_DYJets.root --weight_name weight_2017_TTJets
./create_weights.py --bkg_file $NTUPLE_DIR/electron_ntuple_2017_DYJets.root --sig_file $NTUPLE_DIR/electron_ntuple_2017_DYJets.root --weight_name weight_2017_DYJets
./create_weights.py --bkg_file $NTUPLE_DIR/electron_ntuple_2016_DYJets.root --sig_file $NTUPLE_DIR/electron_ntuple_2016_DYJets.root --weight_name weight_2016_DYJets
./create_weights.py --bkg_file $NTUPLE_DIR/electron_ntuple_2016_TTJets.root --sig_file $NTUPLE_DIR/electron_ntuple_2016_DYJets.root --weight_name weight_2016_TTJets
