python number_of_fakes.py
python number_of_fakes.py --pt_cut
python number_of_fakes.py --only2017
python number_of_fakes.py --pt_cut --only2017
python number_of_fakes.py --ul2017
python number_of_fakes.py --pt_cut --ul2017
python number_of_fakes.py --pt
python number_of_fakes.py --only2017 --pt
python number_of_fakes.py --ul2017 --pt

python number_of_fakes.py --only2017 --tracker_driven
python number_of_fakes.py --pt_cut --only2017 --tracker_driven
python number_of_fakes.py --only2017 --pt --tracker_driven

python number_of_fakes.py --only2017 --ecal_driven
python number_of_fakes.py --pt_cut --only2017 --ecal_driven
python number_of_fakes.py --only2017 --pt --ecal_driven



python number_of_fakes.py --barrel
python number_of_fakes.py --pt_cut --barrel
python number_of_fakes.py --only2017 --barrel
python number_of_fakes.py --pt_cut --only2017 --barrel
python number_of_fakes.py --ul2017 --barrel
python number_of_fakes.py --pt_cut --ul2017 --barrel
python number_of_fakes.py --pt --barrel
python number_of_fakes.py --only2017 --pt --barrel
python number_of_fakes.py --ul2017 --pt --barrel

python number_of_fakes.py --only2017 --tracker_driven --barrel
python number_of_fakes.py --pt_cut --only2017 --tracker_driven --barrel
python number_of_fakes.py --only2017 --pt --tracker_driven --barrel

python number_of_fakes.py --only2017 --ecal_driven --barrel
python number_of_fakes.py --pt_cut --only2017 --ecal_driven --barrel
python number_of_fakes.py --only2017 --pt --ecal_driven --barrel



python number_of_fakes.py --endcap
python number_of_fakes.py --pt_cut --endcap
python number_of_fakes.py --only2017 --endcap
python number_of_fakes.py --pt_cut --only2017 --endcap
python number_of_fakes.py --ul2017 --endcap
python number_of_fakes.py --pt_cut --ul2017 --endcap
python number_of_fakes.py --pt --endcap
python number_of_fakes.py --only2017 --pt --endcap
python number_of_fakes.py --ul2017 --pt --endcap

python number_of_fakes.py --only2017 --tracker_driven --endcap
python number_of_fakes.py --pt_cut --only2017 --tracker_driven --endcap
python number_of_fakes.py --only2017 --pt --tracker_driven --endcap

python number_of_fakes.py --only2017 --ecal_driven --endcap
python number_of_fakes.py --pt_cut --only2017 --ecal_driven --endcap
python number_of_fakes.py --only2017 --pt --ecal_driven --endcap
