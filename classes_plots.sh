#!/bin/bash

python electron_classes.py --ele_file ntuples/electron_ntuple_2016_DYJets.root
python electron_classes.py --ele_file ntuples/electron_ntuple_2017_DYJets.root
python electron_classes.py --ele_file ntuples/electron_ntuple_2016_TTJets.root
python electron_classes.py --ele_file ntuples/electron_ntuple_2017_TTJets.root
