import ROOT
import numpy as np
from root_numpy import tree2array
import matplotlib.pyplot as plt
import argparse
import os
from matplotlib.ticker import FormatStrFormatter

parser = argparse.ArgumentParser()

parser.add_argument("--pt_name", help="pt branch name", default="ele_pt")
parser.add_argument("--eta_name", help="eta branch name", default="scl_eta")

# parser.print_help()

args = parser.parse_args()

# stop = 10000
# stop = 1000000
# stop = 10000000
stop = None

sample_name = "DYJets_2017"

plot_dir = "plots/classes"

sel = "ele_pt >= 5. && abs(scl_eta) < 2.5 && genNpu > 1"

root_file = ROOT.TFile("./ntuples/electron_ntuple_homebrew.root", "READ")
root_dir = root_file.Get("ntuplizer")
root_tree = root_dir.Get("tree")
root_tree_event = root_dir.Get("tree_event")
n_events = root_tree_event.GetEntries()

root_file_mask = ROOT.TFile("./ntuples/electron_ntuple_homebrew_mask.root", "READ")
root_dir_mask = root_file_mask.Get("ntuplizer")
root_tree_mask = root_dir_mask.Get("tree")
root_tree_event_mask = root_dir_mask.Get("tree_event")
n_events_mask = root_tree_event_mask.GetEntries()

ele = tree2array(root_tree, selection=sel, branches=[args.pt_name, args.eta_name, "matchedToGenEle"], stop=stop)
ele_mask = tree2array(
    root_tree_mask, selection=sel, branches=[args.pt_name, args.eta_name, "matchedToGenEle"], stop=stop
)

ind = np.array(range(5))

hist, bins, _ = plt.hist(ele["matchedToGenEle"], density=False, bins=range(6), histtype="bar")  # , color='#7d99d1')
hist_mask, bins_mask, _ = plt.hist(
    ele_mask["matchedToGenEle"], density=False, bins=range(6)
)  # , histtype='bar', color='#cf5e61')
plt.close()

labels = ["unmatched", "prompt", "from taus", "non-prompt", "b-fake"]
width = 0.35  # the width of the bars
ind = np.array(range(len(labels)))

plt.figure(figsize=(4.8, 3.6))
ax = plt.gca()
plt.title("Reconstructed electrons in {}".format(sample_name.replace("Jets", "+Jets").replace("_2017", "")))
rects1 = plt.bar(ind - width / 2.0, hist * 1.0, width)  # , color='#7d99d1')
rects2 = plt.bar(ind + width / 2.0, hist_mask * 1.0, width)  # , color='#cf5e61')
plt.errorbar(ind - width / 2.0, hist * 1, yerr=np.sqrt(hist) * 1, fmt=",", color="k", zorder=999)
plt.errorbar(ind + width / 2.0, hist_mask * 1, yerr=np.sqrt(hist_mask) * 1, fmt=",", color="k", zorder=999)
plt.legend((rects1[0], rects2[0]), ("2017", "2017 Masked 1st Pix"), loc="upper right")
# plt.ylabel("Fraction [%]")
plt.ylabel("Reconstructed Electrons in {} Events".format(n_events))
ax.set_xticks(ind)
ax.set_xticklabels(labels)
plt.xticks(rotation=45)
# plt.ylim(0, 0.25)
plt.savefig(os.path.join(plot_dir, "pix_mask_{}_classes.png".format(sample_name)))
plt.savefig(os.path.join(plot_dir, "pix_mask_{}_classes.pdf".format(sample_name)))
# plt.show()
