import ROOT
import numpy as np
from root_numpy import tree2array
import matplotlib.pyplot as plt
import argparse
import os
from matplotlib.ticker import FormatStrFormatter

parser = argparse.ArgumentParser()

parser.add_argument("--ele_file", help="input file", required=True)
parser.add_argument("--pt_name", help="pt branch name", default="ele_pt")
parser.add_argument("--eta_name", help="eta branch name", default="scl_eta")

# parser.print_help()

args = parser.parse_args()

# stop = 10000
# stop = 1000000
# stop = 10000000
stop = None

sample_name = "DYJets_2017"
year = 2017
if "TTJets" in args.ele_file:
    sample_name = "TTJets_2017"

if "2016" in args.ele_file:
    year = 2016

plot_dir = "plots/classes"

sel_EB = "ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) < 1.479 && vtxN > 1"
sel_EE = "ele_pt < 250 && ele_pt >= 20. && abs(scl_eta) >= 1.479 && abs(scl_eta) < 2.5 && vtxN > 1"

root_file = ROOT.TFile(args.ele_file, "READ")
root_dir = root_file.Get("ntuplizer")
root_tree = root_dir.Get("tree")
root_tree_event = root_dir.Get("tree_event")
n_events = root_tree_event.GetEntries()
n_eles_EB_tot = root_tree.GetEntries(sel_EB)
n_eles_EE_tot = root_tree.GetEntries(sel_EE)

ele = tree2array(
    root_tree,
    selection="(" + sel_EB + ") || (" + sel_EE + ")",
    branches=[args.pt_name, args.eta_name, "matchedToGenEle"],
    stop=stop,
)
ele_EB = tree2array(root_tree, selection=sel_EB, branches=[args.pt_name, args.eta_name, "matchedToGenEle"], stop=stop)
ele_EE = tree2array(root_tree, selection=sel_EE, branches=[args.pt_name, args.eta_name, "matchedToGenEle"], stop=stop)

ind = np.array(range(5))

hist_EB, bins_EB, _ = plt.hist(ele_EB["matchedToGenEle"], density=True, bins=range(6), histtype="bar", color="#7d99d1")
hist_EE, bins_EE, _ = plt.hist(ele_EE["matchedToGenEle"], density=True, bins=range(6), histtype="bar", color="#cf5e61")
plt.close()

labels = ["unmatched", "prompt", "from taus", "non-prompt", "b-fake"]
width = 0.35  # the width of the bars
ind = np.array(range(len(labels)))

plt.figure(figsize=(4.8, 3.6))
ax = plt.gca()
plt.title("Reconstructed electrons in {0} {1}".format(year, sample_name.replace("Jets", "+Jets").replace("_2017", "")))
rects1 = plt.bar(ind - width / 2.0, hist_EB * 1.0 * n_eles_EB_tot / n_events, width, color="#7d99d1")
rects2 = plt.bar(ind + width / 2.0, hist_EE * 1.0 * n_eles_EE_tot / n_events, width, color="#cf5e61")
plt.legend((rects1[0], rects2[0]), ("EB", "EE"), loc="upper right")
# plt.ylabel("Fraction [%]")
plt.ylabel("Reconstructed Electrons per Event")
ax.set_xticks(ind)
ax.set_xticklabels(labels)
plt.xticks(rotation=45)
plt.savefig(os.path.join(plot_dir, "{}_classes.png".format(sample_name)))
plt.savefig(os.path.join(plot_dir, "{}_classes.pdf".format(sample_name)))

##################################
# Draw the kinematic distributions
##################################

if "TTJets" in sample_name:
    labels = ["unmatched", "prompt", "from taus", "non-prompt", "b-fake"]
if "DYJets" in sample_name:
    labels = ["unmatched", "prompt", "from taus", "non-prompt"]

yedges = np.array(list(range(20, 51))[::1] + [55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 90.0, 100.0, 150.0, 200.0, 250.0])
xedges = np.linspace(-2.5, 2.5, 50)
pt_min, pt_max = min(yedges), max(yedges)
eta_min, eta_max = min(xedges), max(xedges)


for var_name, edges in zip([args.pt_name, args.eta_name], [yedges, xedges]):

    bin_centers = edges[:-1] + np.diff(edges) / 2.0

    hist_density = {}
    hist_density_err = {}

    plt.figure()
    ax = plt.gca()

    for i, l in enumerate(labels):
        x = ele[ele["matchedToGenEle"] == i][var_name]
        # hist, _, _ = plt.hist(x, edges, histtype='step', label="signal")
        hist_density[l], _, _ = plt.hist(x, edges, density=True, histtype="step", label=l)
        # hist_err = np.sqrt(hist)
        # hist_density_err[l] = hist_err * hist_density[l] / hist

    if var_name == "ele_pt":
        plt.semilogx([], [], "k-")
        plt.setp(ax.get_xticklabels(minor=True), visible=True)
        plt.tick_params(axis="x", which="minor")

        ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f"))
        ax.xaxis.set_minor_formatter(FormatStrFormatter("%.0f"))

    # for i, l in enumerate(labels):
    # plt.errorbar(bin_centers, hist_density[l], yerr=hist_density_err[l], fmt='o', label=l, markersize="3")

    plt.xlabel(var_name)
    plt.title(
        "Reconstructed electrons in {0} {1}".format(year, sample_name.replace("Jets", "+Jets").replace("_2017", ""))
    )
    plt.legend()
    fig_name = os.path.join(".", "{0}_{1}".format(sample_name, var_name))
    plt.savefig(os.path.join(plot_dir, fig_name + ".png"))
    plt.savefig(os.path.join(plot_dir, fig_name + ".pdf"))
    plt.close()
