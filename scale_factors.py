import ROOT
import numpy as np
from root_numpy import tree2array, array2root, hist2array

scale_factors = {}


def load_scale_factors(sf_label, filename):
    root_sf = ROOT.TFile(filename, "READ")
    sf_hist = root_sf.Get("EGamma_SF2D")

    sf, edges = hist2array(sf_hist, return_edges=True)
    eta_edges, pt_edges = edges

    scale_factors[sf_label] = {}
    scale_factors[sf_label]["values"] = sf
    scale_factors[sf_label]["eta_edges"] = eta_edges
    scale_factors[sf_label]["pt_edges"] = pt_edges


def get_scale_factors(eta, pt, sf_label):
    eta_ind = np.digitize(eta, scale_factors[sf_label]["eta_edges"]) - 1
    pt_ind = np.digitize(pt, scale_factors[sf_label]["pt_edges"]) - 1
    return scale_factors[sf_label]["values"][eta_ind, pt_ind]


load_scale_factors("Fall17V1medium", "./scale_factors/egammaEffi.txt_EGM2D_runBCDEF_passingMedium94X.root")
load_scale_factors("Fall17V1loose", "./scale_factors/egammaEffi.txt_EGM2D_runBCDEF_passingLoose94X.root")
load_scale_factors("Fall17V1tight", "./scale_factors/egammaEffi.txt_EGM2D_runBCDEF_passingTight94X.root")
load_scale_factors("Fall17V1veto", "./scale_factors/egammaEffi.txt_EGM2D_runBCDEF_passingVeto94X.root")
load_scale_factors("Fall17isoV1wp80", "./scale_factors/gammaEffi.txt_EGM2D_runBCDEF_passingMVA94Xwp80iso.root")
load_scale_factors("Fall17isoV1wp90", "./scale_factors/gammaEffi.txt_EGM2D_runBCDEF_passingMVA94Xwp90iso.root")
load_scale_factors("Fall17noIsoV1wp80", "./scale_factors/gammaEffi.txt_EGM2D_runBCDEF_passingMVA94Xwp80noiso.root")
load_scale_factors("Fall17noIsoV1wp90", "./scale_factors/gammaEffi.txt_EGM2D_runBCDEF_passingMVA94Xwp90noiso.root")
